﻿using Kanin_Server.Models;
using Kanin_Server.Models.Bots;
using Kanin_Server.Models.Entities;
using Kanin_Server.Models.Requests;
using Kanin_Server.Models.Responses;
using Kanin_Server.Services;
using Microsoft.AspNetCore.SignalR;
using System.Collections.Concurrent;
using System.Numerics;
using System.Reflection;
using System.Text.Json;

namespace Kanin_Server.Hubs
{
    public class GameHub : Hub
    {
        public const int MAX_PLAYERS = 5;
        private static ConcurrentDictionary<string, User> _connections = new ConcurrentDictionary<string, User>();

        private KaninContext _db;
        private readonly ILogger<GameHub> _logger;
        private readonly LobbyService _lobbyService;
        private readonly GameService _gameService;

        public GameHub(KaninContext context, ILogger<GameHub> logger, IHubContext<GameHub> hubContext, IServiceScopeFactory serviceScopeFactory)
        {
            _db = context;
            _logger = logger;
            var botService = new BotService(hubContext, serviceScopeFactory, logger);
            _gameService = new GameService(this, new ScoreService(context, logger), logger, hubContext, botService);
            _lobbyService = new LobbyService(this, _gameService, botService, logger);
        }

        public override Task OnConnectedAsync()
        {
            HttpContext? httpContext = GetHttpContextExtensions.GetHttpContext(Context);

            if (httpContext == null)
            {
                _logger.LogWarning("ConnectionId {id} is not authorized. Aborting connection", Context.ConnectionId);
                Context.Abort();
                return base.OnConnectedAsync();
            }
            
            string accessToken = httpContext.Request.Headers["accessToken"];
            UserLogin? userlogin = _db.Users.SingleOrDefault(u => u.AccessToken == accessToken);

            if (userlogin == null)
            {
                _logger.LogWarning("ConnectionId {id} is not authorized. Aborting connection", Context.ConnectionId);
                Context.Abort();
                return base.OnConnectedAsync();
            }

            _connections[Context.ConnectionId] = new User(userlogin);

            _logger.LogInformation("Connected [ username:{userName} | connectionId: {connectionId} ]", userlogin.Name, Context.ConnectionId);
            return base.OnConnectedAsync();
        }

        public override Task OnDisconnectedAsync(Exception? exception)
        {
           User user = _connections[Context.ConnectionId];

            _logger.LogInformation("Disconnected [ username: {username} | connectionId: {connectionId} ]", user.Username, Context.ConnectionId);


           Game? game = _gameService.GetGameWithUser(user);
           if (game != null && !_connections.Any(x => x.Value == user && x.Key != Context.ConnectionId))
            {
                Clients.Group(game.GameName).SendAsync("playerDisconnected", user.Username);
                _logger.LogInformation("{username} got disconnected from game {gameName}", user.Username, game.GameName);
            }

           _connections.TryRemove(Context.ConnectionId, out _);

            return base.OnDisconnectedAsync(exception);
        }

        public async Task<bool> SaveGames()
        {
            if (!_connections[Context.ConnectionId].IsAdmin)
            {
                _logger.LogWarning("{username} tried to notify maintenance without being admin", _connections[Context.ConnectionId].Username);
                return false;
            }

            try
            {
                await _gameService.SaveGames();
                return true;
            }
            catch(Exception e)
            {
                _logger.LogError("Failed to save ongoing games. Exception: " + e.Message);
                return false;
            }
        }

        public async Task SendMessageToAllUsers(string message)
        {
            await Clients.All.SendAsync("messageFromAdmin", message);
        }

        public async Task NotifyUsersOnServerMaintenance(int estimatedDurationMinutes)
        {
            await Clients.All.SendAsync("serverMaintenance", estimatedDurationMinutes);
        }

        public List<string>? GetActiveGames()
        {
            if (!_connections[Context.ConnectionId].IsAdmin)
            {
                _logger.LogWarning("{username} tried get active games without being admin", _connections[Context.ConnectionId].Username);
                return null;
            }

            return _gameService.GetActiveGames();
        }

        public ConcurrentDictionary<string, User> GetConnections()
        {
            return _connections;
        }

        public async Task<string> CreateLobby()
        {
            User user = _connections[Context.ConnectionId];
            return await _lobbyService.CreateLobby(user);
        }

        public async Task<List<AvailableLobbyModel>> GetAvailableLobbies()
        {
            User user = _connections[Context.ConnectionId];
            return await _lobbyService.GetAvailableLobbies(user);   

        }

        public async Task<bool> JoinLobby(JoinLobbyModel model)
        {
            User user = _connections[Context.ConnectionId];
            return await _lobbyService.JoinLobby(model, user);
        }

        public async Task AddBot(AddBotModel model)
        {
            User user = _connections[Context.ConnectionId];
            await _lobbyService.AddBot(model, user);
        }

        public async Task RemoveBot(RemoveBotModel model)
        {
            User user = _connections[Context.ConnectionId];
            await _lobbyService.RemoveBot(model, user);
        }

        public async Task<GameSetup.ForClient?> GetGameSetup(String gameName)
        {
            User user = _connections[Context.ConnectionId];
            return await _lobbyService.GetGameSetup(gameName, user);
        }

        public async Task LeaveLobby(JoinLobbyModel model)
        {
            User user = _connections[Context.ConnectionId];
            await _lobbyService.LeaveLobby(model, user);
        }

        public async Task<bool> StartGame(string gameName)
        {
            User user = _connections[Context.ConnectionId];
            return await _lobbyService.StartGame(gameName, user);
        }

        public GetOpenLobbyModel GetOpenLobby()
        {
            User user = _connections[Context.ConnectionId];
            return _lobbyService.GetOpenLobby(user);
        }

        public GetRunningGameModel GetRunningGame()
        {
            User user = _connections[Context.ConnectionId];
            return _gameService.GetRunningGame(user);
        }

        public async Task<ClientGameState?> GetGameState(string gameName)
        {
            User user = _connections[Context.ConnectionId];
            return await _gameService.GetGameState(gameName, user);
        }

        public async Task PlayCard(string gameName, Card card)
        {
            User user = _connections[Context.ConnectionId];
            await _gameService.PlayCard(gameName, card, user);
        }


        public async Task KabalPass(string gameName)
        {
            User user = _connections[Context.ConnectionId];
            await _gameService.KabalPass(gameName, user);
        }

        public async Task PerformTrumpSuitDraw(string gameName, int pickedIndex)
        {
            User user = _connections[Context.ConnectionId];
            await _gameService.PerformTrumpSuitDraw(gameName, pickedIndex, user);
        }

    }
}
