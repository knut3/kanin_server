﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Kanin_Server.Migrations
{
    public partial class initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterDatabase()
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.CreateTable(
                name: "Games",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(type: "varchar(255)", maxLength: 255, nullable: false)
                        .Annotation("MySql:CharSet", "utf8mb4")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Games", x => x.Id);
                })
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.CreateTable(
                name: "RoundScores",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    GameId = table.Column<int>(type: "int", nullable: false),
                    Username = table.Column<string>(type: "varchar(255)", maxLength: 255, nullable: false)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    RoundName = table.Column<string>(type: "varchar(255)", maxLength: 255, nullable: false)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    Score = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RoundScores", x => x.Id);
                })
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.CreateTable(
                name: "TarotCards",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false),
                    RelativePath = table.Column<string>(type: "longtext", nullable: false)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    Arcana = table.Column<string>(type: "varchar(255)", maxLength: 255, nullable: false)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    Name = table.Column<string>(type: "varchar(255)", maxLength: 255, nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    Suit = table.Column<string>(type: "varchar(255)", maxLength: 255, nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    Value = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TarotCards", x => x.Id);
                })
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.CreateTable(
                name: "Users",
                columns: table => new
                {
                    Name = table.Column<string>(type: "varchar(255)", maxLength: 255, nullable: false)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    PasswordHash = table.Column<string>(type: "varchar(255)", maxLength: 255, nullable: false)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    AccessToken = table.Column<string>(type: "varchar(255)", maxLength: 255, nullable: false)
                        .Annotation("MySql:CharSet", "utf8mb4")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.Name);
                })
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.CreateTable(
                name: "EarnedTarotCards",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Username = table.Column<string>(type: "varchar(255)", maxLength: 255, nullable: false)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    GameId = table.Column<int>(type: "int", nullable: false),
                    Reason = table.Column<string>(type: "varchar(255)", maxLength: 255, nullable: false)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    CardId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EarnedTarotCards", x => x.Id);
                    table.ForeignKey(
                        name: "FK_EarnedTarotCards_TarotCards_CardId",
                        column: x => x.CardId,
                        principalTable: "TarotCards",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                })
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.InsertData(
                table: "TarotCards",
                columns: new[] { "Id", "Arcana", "Name", "RelativePath", "Suit", "Value" },
                values: new object[,]
                {
                    { 1, "Major", "The Fool", "StaticFiles/images/Tarot/Major/00.jpg", null, 0 },
                    { 2, "Major", "The Magician", "StaticFiles/images/Tarot/Major/01.jpg", null, 1 },
                    { 3, "Major", "The High Priestess", "StaticFiles/images/Tarot/Major/02.jpg", null, 2 },
                    { 4, "Major", "The Empress", "StaticFiles/images/Tarot/Major/03.jpg", null, 3 },
                    { 5, "Major", "The Emperor", "StaticFiles/images/Tarot/Major/04.jpg", null, 4 },
                    { 6, "Major", "The Hierophant", "StaticFiles/images/Tarot/Major/05.jpg", null, 5 },
                    { 7, "Major", "The Lovers", "StaticFiles/images/Tarot/Major/06.jpg", null, 6 },
                    { 8, "Major", "The Chariot", "StaticFiles/images/Tarot/Major/07.jpg", null, 7 },
                    { 9, "Major", "Justice", "StaticFiles/images/Tarot/Major/08.jpg", null, 8 },
                    { 10, "Major", "The Hermit", "StaticFiles/images/Tarot/Major/09.jpg", null, 9 },
                    { 11, "Major", "Wheel of Fortune", "StaticFiles/images/Tarot/Major/10.jpg", null, 10 },
                    { 12, "Major", "Strength", "StaticFiles/images/Tarot/Major/11.jpg", null, 11 },
                    { 13, "Major", "The Hanged Man", "StaticFiles/images/Tarot/Major/12.jpg", null, 12 },
                    { 14, "Major", "Death", "StaticFiles/images/Tarot/Major/13.jpg", null, 13 },
                    { 15, "Major", "Temperance", "StaticFiles/images/Tarot/Major/14.jpg", null, 14 },
                    { 16, "Major", "The Devil", "StaticFiles/images/Tarot/Major/15.jpg", null, 15 },
                    { 17, "Major", "The Tower", "StaticFiles/images/Tarot/Major/16.jpg", null, 16 },
                    { 18, "Major", "The Star", "StaticFiles/images/Tarot/Major/17.jpg", null, 17 },
                    { 19, "Major", "The Moon", "StaticFiles/images/Tarot/Major/18.jpg", null, 18 },
                    { 20, "Major", "The Sun", "StaticFiles/images/Tarot/Major/19.jpg", null, 19 },
                    { 21, "Major", "Judgement", "StaticFiles/images/Tarot/Major/20.jpg", null, 20 },
                    { 22, "Major", "The World", "StaticFiles/images/Tarot/Major/21.jpg", null, 21 },
                    { 23, "Minor", null, "StaticFiles/images/Tarot/Minor/Cups/01.jpg", "Cups", 1 },
                    { 24, "Minor", null, "StaticFiles/images/Tarot/Minor/Cups/02.jpg", "Cups", 2 },
                    { 25, "Minor", null, "StaticFiles/images/Tarot/Minor/Cups/03.jpg", "Cups", 3 },
                    { 26, "Minor", null, "StaticFiles/images/Tarot/Minor/Cups/04.jpg", "Cups", 4 },
                    { 27, "Minor", null, "StaticFiles/images/Tarot/Minor/Cups/05.jpg", "Cups", 5 },
                    { 28, "Minor", null, "StaticFiles/images/Tarot/Minor/Cups/06.jpg", "Cups", 6 },
                    { 29, "Minor", null, "StaticFiles/images/Tarot/Minor/Cups/07.jpg", "Cups", 7 },
                    { 30, "Minor", null, "StaticFiles/images/Tarot/Minor/Cups/08.jpg", "Cups", 8 },
                    { 31, "Minor", null, "StaticFiles/images/Tarot/Minor/Cups/09.jpg", "Cups", 9 },
                    { 32, "Minor", null, "StaticFiles/images/Tarot/Minor/Cups/10.jpg", "Cups", 10 },
                    { 33, "Minor", null, "StaticFiles/images/Tarot/Minor/Cups/11.jpg", "Cups", 11 },
                    { 34, "Minor", null, "StaticFiles/images/Tarot/Minor/Cups/12.jpg", "Cups", 12 },
                    { 35, "Minor", null, "StaticFiles/images/Tarot/Minor/Cups/13.jpg", "Cups", 13 },
                    { 36, "Minor", null, "StaticFiles/images/Tarot/Minor/Cups/14.jpg", "Cups", 14 },
                    { 37, "Minor", null, "StaticFiles/images/Tarot/Minor/Pentacles/01.jpg", "Pentacles", 1 },
                    { 38, "Minor", null, "StaticFiles/images/Tarot/Minor/Pentacles/02.jpg", "Pentacles", 2 },
                    { 39, "Minor", null, "StaticFiles/images/Tarot/Minor/Pentacles/03.jpg", "Pentacles", 3 },
                    { 40, "Minor", null, "StaticFiles/images/Tarot/Minor/Pentacles/04.jpg", "Pentacles", 4 },
                    { 41, "Minor", null, "StaticFiles/images/Tarot/Minor/Pentacles/05.jpg", "Pentacles", 5 },
                    { 42, "Minor", null, "StaticFiles/images/Tarot/Minor/Pentacles/06.jpg", "Pentacles", 6 },
                    { 43, "Minor", null, "StaticFiles/images/Tarot/Minor/Pentacles/07.jpg", "Pentacles", 7 },
                    { 44, "Minor", null, "StaticFiles/images/Tarot/Minor/Pentacles/08.jpg", "Pentacles", 8 },
                    { 45, "Minor", null, "StaticFiles/images/Tarot/Minor/Pentacles/09.jpg", "Pentacles", 9 },
                    { 46, "Minor", null, "StaticFiles/images/Tarot/Minor/Pentacles/10.jpg", "Pentacles", 10 },
                    { 47, "Minor", null, "StaticFiles/images/Tarot/Minor/Pentacles/11.jpg", "Pentacles", 11 },
                    { 48, "Minor", null, "StaticFiles/images/Tarot/Minor/Pentacles/12.jpg", "Pentacles", 12 },
                    { 49, "Minor", null, "StaticFiles/images/Tarot/Minor/Pentacles/13.jpg", "Pentacles", 13 },
                    { 50, "Minor", null, "StaticFiles/images/Tarot/Minor/Pentacles/14.jpg", "Pentacles", 14 },
                    { 51, "Minor", null, "StaticFiles/images/Tarot/Minor/Swords/01.jpg", "Swords", 1 },
                    { 52, "Minor", null, "StaticFiles/images/Tarot/Minor/Swords/02.jpg", "Swords", 2 },
                    { 53, "Minor", null, "StaticFiles/images/Tarot/Minor/Swords/03.jpg", "Swords", 3 },
                    { 54, "Minor", null, "StaticFiles/images/Tarot/Minor/Swords/04.jpg", "Swords", 4 },
                    { 55, "Minor", null, "StaticFiles/images/Tarot/Minor/Swords/05.jpg", "Swords", 5 },
                    { 56, "Minor", null, "StaticFiles/images/Tarot/Minor/Swords/06.jpg", "Swords", 6 },
                    { 57, "Minor", null, "StaticFiles/images/Tarot/Minor/Swords/07.jpg", "Swords", 7 },
                    { 58, "Minor", null, "StaticFiles/images/Tarot/Minor/Swords/08.jpg", "Swords", 8 },
                    { 59, "Minor", null, "StaticFiles/images/Tarot/Minor/Swords/09.jpg", "Swords", 9 },
                    { 60, "Minor", null, "StaticFiles/images/Tarot/Minor/Swords/10.jpg", "Swords", 10 },
                    { 61, "Minor", null, "StaticFiles/images/Tarot/Minor/Swords/11.jpg", "Swords", 11 },
                    { 62, "Minor", null, "StaticFiles/images/Tarot/Minor/Swords/12.jpg", "Swords", 12 },
                    { 63, "Minor", null, "StaticFiles/images/Tarot/Minor/Swords/13.jpg", "Swords", 13 },
                    { 64, "Minor", null, "StaticFiles/images/Tarot/Minor/Swords/14.jpg", "Swords", 14 },
                    { 65, "Minor", null, "StaticFiles/images/Tarot/Minor/Wands/01.jpg", "Wands", 1 },
                    { 66, "Minor", null, "StaticFiles/images/Tarot/Minor/Wands/02.jpg", "Wands", 2 },
                    { 67, "Minor", null, "StaticFiles/images/Tarot/Minor/Wands/03.jpg", "Wands", 3 },
                    { 68, "Minor", null, "StaticFiles/images/Tarot/Minor/Wands/04.jpg", "Wands", 4 },
                    { 69, "Minor", null, "StaticFiles/images/Tarot/Minor/Wands/05.jpg", "Wands", 5 },
                    { 70, "Minor", null, "StaticFiles/images/Tarot/Minor/Wands/06.jpg", "Wands", 6 },
                    { 71, "Minor", null, "StaticFiles/images/Tarot/Minor/Wands/07.jpg", "Wands", 7 },
                    { 72, "Minor", null, "StaticFiles/images/Tarot/Minor/Wands/08.jpg", "Wands", 8 },
                    { 73, "Minor", null, "StaticFiles/images/Tarot/Minor/Wands/09.jpg", "Wands", 9 },
                    { 74, "Minor", null, "StaticFiles/images/Tarot/Minor/Wands/10.jpg", "Wands", 10 },
                    { 75, "Minor", null, "StaticFiles/images/Tarot/Minor/Wands/11.jpg", "Wands", 11 },
                    { 76, "Minor", null, "StaticFiles/images/Tarot/Minor/Wands/12.jpg", "Wands", 12 },
                    { 77, "Minor", null, "StaticFiles/images/Tarot/Minor/Wands/13.jpg", "Wands", 13 },
                    { 78, "Minor", null, "StaticFiles/images/Tarot/Minor/Wands/14.jpg", "Wands", 14 }
                });

            migrationBuilder.CreateIndex(
                name: "IX_EarnedTarotCards_CardId",
                table: "EarnedTarotCards",
                column: "CardId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "EarnedTarotCards");

            migrationBuilder.DropTable(
                name: "Games");

            migrationBuilder.DropTable(
                name: "RoundScores");

            migrationBuilder.DropTable(
                name: "Users");

            migrationBuilder.DropTable(
                name: "TarotCards");
        }
    }
}
