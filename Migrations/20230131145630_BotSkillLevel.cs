﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Kanin_Server.Migrations
{
    public partial class BotSkillLevel : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "BotSkillLevels",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    GameId = table.Column<int>(type: "int", nullable: false),
                    BotName = table.Column<string>(type: "varchar(255)", maxLength: 255, nullable: false)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    SkillLevel = table.Column<string>(type: "varchar(255)", maxLength: 255, nullable: false)
                        .Annotation("MySql:CharSet", "utf8mb4")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BotSkillLevels", x => x.Id);
                })
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.UpdateData(
                table: "TarotCards",
                keyColumn: "Id",
                keyValue: 9,
                column: "Name",
                value: "Strength");

            migrationBuilder.UpdateData(
                table: "TarotCards",
                keyColumn: "Id",
                keyValue: 12,
                column: "Name",
                value: "Justice");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "BotSkillLevels");

            migrationBuilder.UpdateData(
                table: "TarotCards",
                keyColumn: "Id",
                keyValue: 9,
                column: "Name",
                value: "Justice");

            migrationBuilder.UpdateData(
                table: "TarotCards",
                keyColumn: "Id",
                keyValue: 12,
                column: "Name",
                value: "Strength");
        }
    }
}
