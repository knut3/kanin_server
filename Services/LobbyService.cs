﻿using Kanin_Server.Hubs;
using Kanin_Server.Models;
using Kanin_Server.Models.Bots;
using Kanin_Server.Models.Requests;
using Kanin_Server.Models.Responses;
using Microsoft.AspNetCore.SignalR;
using System.Collections.Concurrent;
using System.Text.RegularExpressions;
using static Microsoft.EntityFrameworkCore.DbLoggerCategory.Database;

namespace Kanin_Server.Services
{
    public class LobbyService
    {
        private static ConcurrentDictionary<string, GameSetup> _lobbies = new ConcurrentDictionary<string, GameSetup>();

        private readonly GameHub _hub;
        private readonly GameService _gameService;
        private readonly BotService _botService;
        private readonly ILogger _logger;

        public LobbyService(GameHub hub, GameService gameService, BotService botService, ILogger logger)
        {
            _hub = hub;
            _gameService = gameService;
            _botService = botService;
            _logger = logger;
        }

        public GetOpenLobbyModel GetOpenLobby(User user)
        {
            GameSetup? lobby = _lobbies.Values.SingleOrDefault(l => l.Users.Contains(user));

            if (lobby == null)
            {
                _logger.LogInformation("No open lobby found where {username} is member", user.Username);
                return new GetOpenLobbyModel { FoundResult = false };
            }
            else if (lobby.Creator == user)
            {
                _logger.LogInformation("Returning open lobby {gameName} where {username} is Creator", lobby.GameName, user.Username);
                return new GetOpenLobbyModel { FoundResult = true, GameName = lobby.GameName, IsCreator = true };
            }
            else
            {
                _logger.LogInformation("Returning open lobby {gameName} to {username}", lobby.GameName, user.Username);
                return new GetOpenLobbyModel { FoundResult = true, GameName = lobby.GameName, IsCreator = false };
            }
        }

        public async Task<string> CreateLobby(User user)
        {
            string gameName = user.Username;

            await leaveOldLobbyIfExists(user);

            if (GameService.GameExists(gameName))
            {
                await _gameService.CloseGame(gameName);
                _logger.LogInformation("CreateLobby: Closed old game {gameName}", gameName);
            }

            GameSetup gameSetup = new GameSetup
            {
                GameName = gameName,
                Creator = user
            };

            gameSetup.Users.Add(user);
            _lobbies[gameName] = gameSetup;
            GameService.CreateGameLock(gameName);

            await _hub.Groups.AddToGroupAsync(_hub.Context.ConnectionId, gameName);
            await _hub.Clients.All.SendAsync("lobbyCreated", new AvailableLobbyModel { Name = gameName, PlayerCount = 1 });

            _logger.LogInformation("Lobby {gameName} created", gameName);

            return gameName;
        }

        public Task<List<AvailableLobbyModel>> GetAvailableLobbies(User user)
        {

            List<AvailableLobbyModel> lobbies = _lobbies.Values.Where(setup => setup.GetPlayersCount() <= GameHub.MAX_PLAYERS)
                .Select(setup => new AvailableLobbyModel
            {
                Name = setup.GameName,
                PlayerCount = setup.GetPlayersCount()
            }).ToList();


            _logger.LogInformation("Returning {num} available lobbies to {username}",
                lobbies.Count, user.Username);


            return Task.FromResult(lobbies);
        }

        public async Task<bool> JoinLobby(JoinLobbyModel model, User user)
        {
            GameSetup? lobby;
            _lobbies.TryGetValue(model.GameName, out lobby);
            if (lobby == null)
            {
                _logger.LogWarning("{username} tried to join unexisting lobby {gameName}", user.Username, model.GameName);
                return false;
            }

            var gameLock = GameService.GetGameLock(model.GameName);
            await gameLock.WaitAsync();

            if (lobby.Users.Contains(user))
            {
                await _hub.Groups.AddToGroupAsync(_hub.Context.ConnectionId, lobby.GameName);
                _logger.LogInformation("{username} rejoined lobby. Connection added to hubgroup {gameName}", user.Username, lobby.GameName);
                return true;
            }

            if (lobby.GetPlayersCount() >= GameHub.MAX_PLAYERS)
            {
                _logger.LogWarning("{username} tried to join full lobby {gameName}", user.Username, lobby.GameName);
                if (lobby.GetPlayersCount() > GameHub.MAX_PLAYERS)
                    _logger.LogError("Somehow number of players has managed to exceed max capacity");
                return false;
            }

            await leaveOldLobbyIfExists(user);

            lobby.Users.Add(user);
            gameLock.Release();

            await _hub.Clients.Group(lobby.GameName).SendAsync("userJoined", user.Username);
            await _hub.Groups.AddToGroupAsync(_hub.Context.ConnectionId, lobby.GameName);

            _logger.LogInformation("{username}:{connectionId} joined lobby {gameName}", user.Username, _hub.Context.ConnectionId, lobby.GameName);
            return true;
        }

        public async Task AddBot(AddBotModel model, User user)
        {
            GameSetup? lobby;
            _lobbies.TryGetValue(model.GameName, out lobby);
            if (lobby == null)
            {
                _logger.LogWarning("{username} tried to add bot to unexisting lobby {gameName}", user.Username, model.GameName);
                return;
            }

            if (lobby.Creator != user)
            {
                _logger.LogWarning("{username} tried to add bot to lobby {gameName}, but is not authorized", user.Username, model.GameName);
                return;
            }

            var gameLock = GameService.GetGameLock(lobby.GameName);
            await gameLock.WaitAsync();

            if (lobby.GetPlayersCount() >= GameHub.MAX_PLAYERS)
            {
                _logger.LogWarning("{username} tried to add bot to full lobby {gameName}", user.Username, lobby.GameName);
                if (lobby.GetPlayersCount() > GameHub.MAX_PLAYERS)
                    _logger.LogError("Somehow number of players has managed to exceed max capacity");
                return;
            }

            if (model.Intelligence != BotModel.DUMB && BotModel.MEDIUM != model.Intelligence)
            {
                _logger.LogWarning("{username} tried to add a bot with unknown intelligence level ({intelligence})", user.Username, model.Intelligence);
                return;
            }

            string botName = BotService.GetBotName(lobby.Bots.Select(x => x.Name).ToArray());
            var botModel = new BotModel { Name = botName, Intelligence = model.Intelligence };
            lobby.Bots.Add(botModel);

            gameLock.Release();

            await _hub.Clients.Group(lobby.GameName).SendAsync("botAdded", botModel);
            _logger.LogInformation("{botName} added to lobby {gameName}", botName, model.GameName);
        }

        public async Task RemoveBot(RemoveBotModel model, User user)
        {
            GameSetup? lobby;
            _lobbies.TryGetValue(model.GameName, out lobby);

            if (lobby == null)
            {
                _logger.LogWarning("{username} tried to remove bot from unexisting lobby {gameName}", user.Username, model.GameName);
                return;
            }

            if (user != lobby.Creator)
            {
                _logger.LogWarning("{username} is not authorized to remove bot for game {gameName}", user.Username, model.GameName);
                return;
            }

            var gameLock = GameService.GetGameLock(lobby.GameName);
            await gameLock.WaitAsync();

            BotModel? bot = lobby.Bots.SingleOrDefault(x => x.Name == model.BotName);

            if (bot == null)
            {
                _logger.LogWarning("{username} tried to remove unexisting {botName} from {gameName}", user.Username, model.BotName, model.GameName);
                return;
            }

            lobby.Bots.Remove(bot);

            gameLock.Release();

            await _hub.Clients.Group(model.GameName).SendAsync("botRemoved", model.BotName);
        }

        public async Task<GameSetup.ForClient?> GetGameSetup(String gameName, User user)
        {

            if (!_lobbies.ContainsKey(gameName))
            {
                if (GameService.GameExists(gameName))
                {
                    _logger.LogInformation("{username} requested game setup of already started game {gameName}. Returning that info.",
                        _hub.GetConnections()[_hub.Context.ConnectionId].Username, gameName);
                    return GameSetup.ToClientModelGameHasStarted(gameName);
                }
                else
                {
                    _logger.LogWarning("GetGameSetup: Lobby {gameName} does not exist", gameName);
                    return null;
                }
            }

            var gameLock = GameService.GetGameLock(gameName);
            await gameLock.WaitAsync();

            GameSetup lobby = _lobbies[gameName];

            gameLock.Release();

            if (!lobby.Users.Contains(user))
            {
                _logger.LogWarning("GetGameSetup: User {username} has not joined lobby {gameName}", user.Username, gameName);
                return null;
            }

            await _hub.Groups.AddToGroupAsync(_hub.Context.ConnectionId, gameName);

            _logger.LogInformation("Returning game setup for lobby {gameName} to {username}", gameName, user.Username);
            return lobby.ToClientModel();
        }

        public async Task LeaveLobby(JoinLobbyModel model, User user)
        {
            if (!_lobbies.ContainsKey(model.GameName))
            {
                _logger.LogWarning("{username} tried to leave a lobby ({gameName}) that does not exist",
                    user.Username, model.GameName);
                return;
            }

            var gameLock = GameService.GetGameLock(model.GameName);
            await gameLock.WaitAsync();

            GameSetup lobby = _lobbies[model.GameName];

            if (!lobby.Users.Contains(user))
            {
                _logger.LogWarning("LeaveLobby: No reason to leave lobby {gameName}. User {username} is not a player in the lobby.",
                    model.GameName, user.Username);
                return;
            }

            await handleLeaveLobby(lobby, user);
            gameLock.Release();
        }

        public async Task<bool> StartGame(string gameName, User user)
        {
            GameSetup? gameSetup = _lobbies.GetValueOrDefault(gameName);

            if (gameSetup == null)
            {
                _logger.LogWarning("{username} tried to start a game ({gameName}) that does not exist", user.Username, gameName);
                return false;
            }

            if (user != gameSetup.Creator)
            {
                _logger.LogWarning("{username} tried to start game {gameName} in which he is not the creator", user.Username, gameName);
                return false;
            }

            int playerCount = gameSetup.GetPlayersCount();
            if (playerCount < 3 || 5 < playerCount)
            {
                _logger.LogWarning("{username} tried to start a game with {num} players and {bots} bots", user.Username, gameSetup.Users.Count, gameSetup.Bots.Count);
                return false;
            }

            await _gameService.ClearOldGames(gameSetup.Users);
            Game game = _gameService.CreateGame(gameSetup);
            await _hub.Clients.All.SendAsync("lobbyUnavailable", gameName);
            _lobbies.TryRemove(gameName, out _);

            if (game.Rounds.GetCurrentRound().Name != Rounds.Trumf)
                _botService.PossiblyPerformBotActions_NoDelay(game);

            await _hub.Clients.Group(gameName).SendAsync("gameStarting");
            _logger.LogInformation("Game {gameName} started", gameName);
            return true;
        }


        private async Task leaveOldLobbyIfExists(User user)
        {
            var oldLobby = _lobbies.Values.SingleOrDefault(x => x.Users.Contains(user));
            if (oldLobby != null)
            {
                await handleLeaveLobby(oldLobby, user);
            }
        }

        private async Task handleLeaveLobby(GameSetup lobby, User user)
        {
            if (user == lobby.Creator)
            {
                _logger.LogInformation("Closing lobby {gameName} because the creator {username}:{connectionId} left",
                    lobby.GameName, user.Username, _hub.Context.ConnectionId);
                await closeLobby(lobby);
            }
            else
            {
                lobby.Users.Remove(user);
                _logger.LogInformation("{username} left lobby {gameName}", user.Username, lobby.GameName);

                await _hub.Groups.RemoveFromGroupAsync(_hub.Context.ConnectionId, lobby.GameName);
                await _hub.Clients.Group(lobby.GameName).SendAsync("userLeft", user.Username);
            }
        }

        private async Task closeLobby(GameSetup lobby)
        {
            await _hub.Clients.Group(lobby.GameName).SendAsync("lobbyClosed");
            await _hub.Clients.All.SendAsync("lobbyUnavailable", lobby.GameName);

            var connections = _hub.GetConnections();

            foreach (var user in lobby.Users)
            {
                var userConnections = connections.Where(c => c.Value == user);
                foreach (var connection in userConnections)
                    await _hub.Groups.RemoveFromGroupAsync(connection.Key, lobby.GameName);
            }

            _lobbies.TryRemove(lobby.GameName, out _);
            GameService.DeleteGameLock(lobby.GameName);
            _logger.LogInformation("Lobby {gameName} closed", lobby.GameName);
        }

    }
}
