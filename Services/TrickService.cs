﻿using Kanin_Server.Models;

namespace Kanin_Server.Services
{
    public class TrickService
    {
        private string roundName;
        private string? trumpSuit;
        public TrickService(string roundName)
        {
            this.roundName = roundName;
        }

        public void SetTrumpSuit(string suit) { trumpSuit = suit; }
        public bool IsValidMove(Card card, List<Card> table, List<Card> hand)
        {
            if (table.Count == 0) return true;
            
            string leadingSuit = table[0].Suit;

            if (card.Suit == leadingSuit) return true;

            else if (!CardService.ContainsSuit(hand, leadingSuit)) return true;

            else return false;
        }

        public TrickResult GetTrickResult(List<CardWithOwner> trick)
        {
            switch (roundName)
            {
                case Rounds.Pass:
                    return new PassService().GetTrickResult(trick);
                case Rounds.Spar:
                    return new SparService().GetTrickResult(trick);
                case Rounds.Damer:
                    return new DamerService().GetTrickResult(trick);
                case Rounds.Grand:
                    return new GrandService().GetTrickResult(trick);
                case Rounds.Trumf:
                    return getTrumpTrickResult(trick);
            }

            throw new Exception("Unable to get TrickResult for round name " + roundName);
        }

        private TrickResult getTrumpTrickResult(List<CardWithOwner> trick)
        {
            if (trumpSuit is null)
            {
                throw new Exception("TrickService: Trump suit not set");
            }
            return new TrumfService().GetTrickResult(trick, trumpSuit);
        }
    }
}
