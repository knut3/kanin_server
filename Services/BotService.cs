﻿using Castle.DynamicProxy.Generators.Emitters.SimpleAST;
using Kanin_Server.Hubs;
using Kanin_Server.Models;
using Kanin_Server.Models.Bots;
using Kanin_Server.Models.Responses;
using Microsoft.AspNetCore.SignalR;

namespace Kanin_Server.Services
{
    public class BotService
    {
        private const int DEFAULT_BOT_ACTION_DELAY_MS = 1500;
        public static readonly string[] BOT_NAMES = { "Bot Anna", "Bot James", "Bot Maria", "Bot Siegfried", "Jon", "Gunnar", "Marie", "Sveinung" };
        private readonly IHubContext<GameHub> _hubContext;
        private readonly IServiceScopeFactory _serviceScopeFactory;
        private readonly ILogger _logger;

        public BotService(IHubContext<GameHub> hubContext, IServiceScopeFactory serviceScopeFactory, ILogger logger)
        {
            this._hubContext = hubContext;
            _serviceScopeFactory = serviceScopeFactory;
            this._logger = logger;
        }

        public static string GetBotName(string[] addedBots)
        {
            var availableNames = BOT_NAMES.Where(x => !addedBots.Contains(x));
            return availableNames.ToArray()[new Random().Next(availableNames.Count())];
        }

        public void PossiblyPerformBotActions_NoDelay(Game game)
        {
            while (true)
            {
                Player nextPlayer = game.Players.Single(x => x.Name == game.NextToPlay);

                if (nextPlayer is HumanPlayer) break;
                Card? played = PerformBotAction(game, nextPlayer);

                if (played == null) // Pass in Kabal
                {
                    game.RoundScores[Rounds.Kabal].AddPoints(nextPlayer.Name, 1);
                    game.MoveToNextPlayerClockwise();
                }

                game.Rounds.GetCurrentRound().HasStarted = true;
            }
        }

        public async Task PossiblyPerformBotActions_AfterCardPlayedByHuman_TrickGame(Game game, SemaphoreSlim gameLock, Func<Player, CardWithOwner, IHubContext<GameHub>, ScoreService, Task> handleTrickEnded)
        {
            await gameLock.WaitAsync();
            while (true)
            {
                Player nextPlayer = game.Players.Single(x => x.Name == game.NextToPlay);

                if (nextPlayer is HumanPlayer) break;

                await Task.Delay(DEFAULT_BOT_ACTION_DELAY_MS);
                Card? playedByBot = PerformBotAction(game, nextPlayer);
                var botCardWithOwner = new CardWithOwner { Card = playedByBot, Owner = nextPlayer.Name };

                if (game.CardsOnTable.Count == game.Players.Count)
                {
                    try
                    {
                        using(var scope = _serviceScopeFactory.CreateScope())
                        {
                            var db = scope.ServiceProvider.GetService<KaninContext>();
                            var scoreService = new ScoreService(db, _logger);
                            await handleTrickEnded(nextPlayer, botCardWithOwner, _hubContext, scoreService);
                        }
                        break;
                    }
                    catch (Exception ex)
                    {
                        _logger.LogWarning(ex.Message);
                        return;
                    }
                }
                else
                {
                    await _hubContext.Clients.Group(game.GameName).SendAsync("cardPlayed", new CardPlayedModel
                    {
                        Card = botCardWithOwner,
                        NextToPlay = game.NextToPlay
                    });

                }
            }
            gameLock.Release();
        }

        public async Task PossiblyPerformBotActions_AfterTrickEnded(Game game, SemaphoreSlim gameLock)
        {
            await gameLock.WaitAsync();
            bool isTrickStarter = true;
            while (true)
            {
                Player nextPlayer = game.Players.Single(x => x.Name == game.NextToPlay);

                if (nextPlayer is HumanPlayer) break;

                int delay = DEFAULT_BOT_ACTION_DELAY_MS;
                if (isTrickStarter)
                {
                    delay = 4000;
                    isTrickStarter = false;
                }

                await Task.Delay(delay);
                Card? playedByBot = PerformBotAction(game, nextPlayer);
                await _hubContext.Clients.Group(game.GameName).SendAsync("cardPlayed", new CardPlayedModel
                {
                    Card = new CardWithOwner { Card = playedByBot, Owner = nextPlayer.Name },
                    NextToPlay = game.NextToPlay
                });

            }
            gameLock.Release();
        }

        public async Task PossiblyPerformBotActions_AfterCardPlayedOrPassByHuman_Kabal(Game game, SemaphoreSlim gameLock, Func<CardWithOwner, Task> endKabalRound)
        {
            await gameLock.WaitAsync();
            while (true)
            {
                Player nextPlayer = game.Players.Single(x => x.Name == game.NextToPlay);

                if (nextPlayer is HumanPlayer) break;

                await Task.Delay(DEFAULT_BOT_ACTION_DELAY_MS);
                Card? playedByBot = PerformBotAction(game, nextPlayer);

                if (playedByBot == null) // Pass in Kabal
                {
                    game.RoundScores[Rounds.Kabal].AddPoints(nextPlayer.Name, 1);
                    game.MoveToNextPlayerClockwise();
                    await _hubContext.Clients.Group(game.GameName).SendAsync("kabalPass", new KabalPassModel
                    {
                        Passer = nextPlayer.Name,
                        NextToPlay = game.NextToPlay
                    });
                }
                else if (nextPlayer.Cards.Count == 0)
                {
                    await endKabalRound(new CardWithOwner { Card = playedByBot, Owner = nextPlayer.Name });
                    break;
                }
                else
                {
                    await _hubContext.Clients.Group(game.GameName).SendAsync("cardPlayed", new CardPlayedModel
                    {
                        Card = new CardWithOwner { Card = playedByBot, Owner = nextPlayer.Name },
                        NextToPlay = game.NextToPlay
                    });
                }
            }
            gameLock.Release();
        }

        public Card? PerformBotAction(Game game, Player bot)
        {
            Card? played = GetBotMove(game.Rounds.GetCurrentRound().Name, (AiPlayer)bot, game);

            if (played == null)
                return null;

            game.CardsOnTable.Add(new CardWithOwner { Card = played, Owner = bot.Name });

            if (played.Value == 1 || played.Value == 14)
                bot.Cards.RemoveAll(x => (x.Value == 1 || 14 == x.Value) && x.Suit == played.Suit);
            else
                bot.Cards.RemoveAll(x => x.Value == played.Value && x.Suit == played.Suit);

            game.MoveToNextPlayerClockwise();

            _logger.LogInformation("{suit}:{value} played by {username} in game {gameName}", played.Suit, played.Value, bot.Name, game.GameName);

            return played;
        }

        public Card? GetBotMove(string roundName, AiPlayer bot, Game game) 
        {
            List<Card> validMoves = roundName == Rounds.Kabal ?
                _getValidKabalGameMoves(bot.Cards, game.CardsOnTable) : _getValidTrickGameMoves(bot.Cards, game.CardsOnTable);

            if (validMoves.Count == 0)
            {
                return null;
            }

            AiPolicy policy = bot.GetPolicy(roundName);
            
            return policy.GetAction(validMoves, game, bot.Cards);
        }

        private List<Card> _getValidTrickGameMoves(List<Card> hand, List<CardWithOwner> table) 
        {
            List<Card> validMoves = new List<Card>(hand.Count);

            if (table.Count == 0)
            {
                validMoves.AddRange(hand);
                return validMoves;
            }

            string trickSuit = table[0].Card.Suit;

            if (!hand.Any(x => x.Suit == trickSuit))
            {
                validMoves.AddRange(hand);
                return validMoves;
            }

            foreach (var card in hand)
            {
                if (card.Suit == trickSuit)
                {
                    validMoves.Add(card);
                }
            }

            return validMoves;
        }

        private List<Card> _getValidKabalGameMoves(List<Card> hand, List<CardWithOwner> table)
        {
            var pureTable = table.Select(x => x.Card).ToList();
            List<Card> validMoves = new List<Card>(hand.Count);

            KabalService kabalService = new KabalService();
            foreach (var card in hand)
            {
                Card possiblyUpdatedCard; //updated 14 if only valid as 1
                if (kabalService.IsValidMove(card, pureTable, out possiblyUpdatedCard))
                {
                    validMoves.Add(possiblyUpdatedCard);
                }
            }

            return validMoves;
        }

    }
}
