﻿using Kanin_Server.Models;

namespace Kanin_Server.Services
{
    public class KabalService
    {
        public bool IsValidMove(Card card, List<Card> cardsOnTable, out Card possiblyUpdatedCard)
        {
            possiblyUpdatedCard = card;

            if (cardsOnTable.Count == 0)
            {
                return card.Suit == Suits.Clubs && card.Value == 7;
            }

            if (card.Value == 7) return true;
            else if (card.Value == 14) 
            {
                if (exist(card.Suit, CourtCards.King, cardsOnTable))
                    return true;
                else if (exist(card.Suit, 2, cardsOnTable))
                {
                    possiblyUpdatedCard.Value = 1;
                    return true;
                }
                else return false;
            }
            else if (card.Value > 7) return exist(card.Suit, card.Value - 1, cardsOnTable);
            else if (card.Value == 6) return isValidSix(card.Suit, cardsOnTable);
            else return exist(card.Suit, card.Value+1, cardsOnTable); // card value less than six
        }

        public List<CardWithOwner> RemoveUnneccesaryCard(Card newCard, List<CardWithOwner> cardsOnTable)
        {
            if (newCard.Value == 7 || 6 == newCard.Value) return cardsOnTable;

            List<CardWithOwner> updatedTable = new List<CardWithOwner>(cardsOnTable);

            if (newCard.Value > 7) updatedTable
                    .RemoveAll(x => x.Card.Value == (newCard.Value - 1) && x.Card.Suit == newCard.Suit);

            else if (newCard.Value < 6) updatedTable
                    .RemoveAll(x => x.Card.Value == (newCard.Value + 1) && x.Card.Suit == newCard.Suit);

            return updatedTable;

        }

        private bool exist(string suit, int value, List<Card> cards)
        {
            foreach (Card card in cards)
            {
                if (card.Suit != suit) continue;

                if (card.Value == value) return true;
            }

            return false;
        }

        private bool isValidSix(string suit, List<Card> cards)
        {
            foreach(Card card in cards)
            {
                if (card.Suit != suit) continue;

                if (card.Value > 7) return true;
            }

            return false;
        }
    }
}
