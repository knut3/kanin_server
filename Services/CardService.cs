﻿using Kanin_Server.Models;

namespace Kanin_Server.Services
{
    public static class CardService
    {
        public static void Deal(List<Player> players)
        {
            players.ForEach(p => p.Cards.Clear());
            int numPlayers = players.Count;
            List<Card> deck = getDeck();
            deck = removeCardSurpluss(deck, numPlayers);
            List<int> deckIndecies = Enumerable.Range(0, deck.Count).ToList();

            Random random = new Random();
            while (deck.Count > 0)
            {
                foreach (Player player in players)
                {
                    int randomIndex = random.Next(0, deck.Count);
                    player.Cards.Add(deck[randomIndex]);
                    deck.RemoveAt(randomIndex);
                }
            }
        }

        public static List<Card> Sort(List<Card> cards)
        {
            var suitOrders = new Dictionary<string, int>(4);
            suitOrders.Add(Suits.Clubs, 1);
            suitOrders.Add(Suits.Diamonds, 2);
            suitOrders.Add(Suits.Spades, 3);
            suitOrders.Add(Suits.Hearts, 4);
            return cards.OrderBy(x => suitOrders[x.Suit]).ThenBy(x => x.Value).ToList();
        }

        public static bool ContainsSuit(List<Card> cards, string suit)
        {
            foreach(Card card in cards)
            {
                if (card.Suit == suit)
                    return true;
            }
            return false;
        }

        public static bool ContainsValue(List<Card> cards, int value)
        {
            foreach (Card card in cards)
            {
                if (card.Value == value)
                    return true;
            }
            return false;
        }

        private static List<Card> removeCardSurpluss(List<Card> deck, int numPlayers)
        {
            if (numPlayers == 3 || numPlayers == 5)
            {
                Card twoOfClubs = deck.Single(x => x.Suit == Suits.Clubs && x.Value == 2);
                deck.Remove(twoOfClubs);
            }

            if (numPlayers == 5)
            {
                Card twoOfDiamonds = deck.Single(x => x.Suit == Suits.Diamonds && x.Value == 2);
                deck.Remove(twoOfDiamonds);
            }

            return deck;
        }

        private static List<Card> getDeck()
        {
            string[] suits = { Suits.Clubs, Suits.Hearts, Suits.Spades, Suits.Diamonds };
            List<Card> cards = new List<Card>();

            foreach (string suit in suits)
            {
                for (int i = 2; i <= 14; i++)
                {
                    cards.Add(new Card(suit, i));
                }
            }
            return cards;
        }
    }
}
