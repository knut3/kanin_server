﻿using Kanin_Server.Models;
using Kanin_Server.Models.Bots;
using Kanin_Server.Models.Entities;
using Kanin_Server.Models.Responses;
using System.Collections.Immutable;
using System.Security.Cryptography.Xml;

namespace Kanin_Server.Services
{
    public class ScoreService
    {
        private readonly KaninContext _db;
        private readonly ILogger _logger;

        public ScoreService(KaninContext db, ILogger logger)
        {
            _db = db;
            _logger = logger;
        }

        public async Task<int> SaveScoresAndReturnGameId(Game game)
        {
            var gameEntity = new GameEntity { Name = game.GameName };
            await _db.Games.AddAsync(gameEntity);
            await _db.SaveChangesAsync();

            var totalScores = new Dictionary<string, int>();
            game.Players.ForEach(p => totalScores.Add(p.Name, 0));

            foreach (var round in game.RoundScores.Values)
            {
                foreach (var roundScore in round.Scores)
                {
                    await _db.RoundScores.AddAsync(new RoundScore
                    {
                        GameId = gameEntity.Id,
                        RoundName = round.RoundName,
                        Username = roundScore.Key,
                        Score = roundScore.Value
                    });

                    totalScores[roundScore.Key] += roundScore.Value;
                }
            }

            createGameResults(totalScores, gameEntity.Id, game.Players.Count());

            await _db.SaveChangesAsync();

            performMagickCardDraw(gameEntity);

            foreach (var player in game.Players)
            {
                if (player is AiPlayer)
                {
                    var aiPlayer = (AiPlayer)player;
                    _db.BotSkillLevels.Add(new BotSkillLevel
                    {
                        GameId = gameEntity.Id,
                        BotName = player.Name,
                        SkillLevel = aiPlayer.Intelligence
                    });
                }
            }

            await _db.SaveChangesAsync();

            _logger.LogInformation("Scores and earned magick cards saved for game {gameName}. Returning GameId", game.GameName);

            return gameEntity.Id;
        }

        public void createGameResults(Dictionary<string, int> totalScores, int gameId, int numPlayers)
        {
            var sorted = from entry in totalScores orderby entry.Value ascending select entry;
            var placements = new List<UserPlacement>();


            int? previousScore = null;
            var withSameScore = new List<UserPlacement>(2);
            int placementSameScoreIgnorant = 1;
            foreach (var playerScore in sorted)
            {
                if (previousScore != null && previousScore != playerScore.Value)
                {
                    placements.AddRange(getUserPlacementsForEqualScore(withSameScore));
                    withSameScore.Clear();
                }

                withSameScore.Add(new UserPlacement
                {
                    Username = playerScore.Key,
                    Score = playerScore.Value,
                    Placement = placementSameScoreIgnorant
                });

                previousScore = playerScore.Value;
                placementSameScoreIgnorant++;
            }

            placements.AddRange(getUserPlacementsForEqualScore(withSameScore));

            foreach (var placement in placements)
            {
                _db.GameResults.Add(new GameResult
                {
                    GameId = gameId,
                    NumPlayers = numPlayers,
                    Username = placement.Username,
                    Score = placement.Score,
                    Placement = placement.Placement
                });
            }
        }

        private List<UserPlacement> getUserPlacementsForEqualScore(List<UserPlacement> sameScore)
        {
            if (sameScore.Count == 1) return sameScore;

            float averagePlacement = sameScore.Average(x => x.Placement);

            sameScore.ForEach(x => x.Placement = averagePlacement);

            return sameScore;
        }

        private void performMagickCardDraw(GameEntity gameEntity)
        {
            var tarotCardDrawer = new TarotCardDrawer(_db);
            var winnerDraw = tarotCardDrawer.draw(7);
            var rabbitDraw = tarotCardDrawer.draw(7);

            if (winnerDraw != null)
            {
                var totalScores = getTotalScores(gameEntity.Id);

                int winnerScore = totalScores.Min(x => x.Score);

                var winners = totalScores.Where(x => x.Score == winnerScore);

                foreach (var winner in winners)
                {
                    if (BotService.BOT_NAMES.Contains(winner.Username))
                        continue;

                    _db.EarnedTarotCards.Add(new EarnedTarotCard
                    {
                        Card = winnerDraw,
                        GameId = gameEntity.Id,
                        Username = winner.Username,
                        Reason = "winner"
                    });
                }
            }

            if (rabbitDraw != null)
            {
                var totalScores = getTotalScores(gameEntity.Id);

                int rabbitScore = totalScores.Max(x => x.Score);

                var rabbits = totalScores.Where(x => x.Score == rabbitScore);

                foreach (var rabbit in rabbits)
                {
                    if (BotService.BOT_NAMES.Contains(rabbit.Username))
                        continue;

                    _db.EarnedTarotCards.Add(new EarnedTarotCard
                    {
                        Card = rabbitDraw,
                        GameId = gameEntity.Id,
                        Username = rabbit.Username,
                        Reason = "rabbit"
                    });
                }
            }
        }

        public List<ScoreModel> getTotalScores(int gameId)
        {
            return _db.RoundScores
                .Where(round => round.GameId == gameId)
                .GroupBy(gr => gr.Username)
                .Select(gru => new ScoreModel
                {
                    Username = gru.Key,
                    Score = gru.Sum(x => x.Score)
                }).ToList();
        }

        private class UserPlacement
        {
            public string Username { get; set; }
            public int Score { get; set; }
            public float Placement { get; set; }
        }
    }
}
