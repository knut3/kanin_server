﻿using Kanin_Server.Models;
using System.Collections.Concurrent;
using Kanin_Server.Hubs;
using Microsoft.AspNetCore.SignalR;
using Kanin_Server.Models.Responses;
using System.Diagnostics.Metrics;
using System.Text.RegularExpressions;
using System.Text.Json;
using Kanin_Server.Models.Bots;

namespace Kanin_Server.Services
{
    public class GameService
    {
        private static ConcurrentDictionary<string, Game> _games = new ConcurrentDictionary<string, Game>();
        private static ConcurrentDictionary<string, SemaphoreSlim> _gameLocks = new ConcurrentDictionary<string, SemaphoreSlim>();
        private readonly GameHub _hub;
        private readonly ScoreService _scoreService;
        private readonly ILogger _logger;
        private readonly BotService _botService;
        private readonly IHubContext<GameHub> _hubContext;

        public GameService(GameHub gameHub, ScoreService scoreService, ILogger logger, 
            IHubContext<GameHub> hubContext, BotService botService)
        {
            _hub = gameHub;
            _scoreService = scoreService;
            _logger = logger;
            _hubContext = hubContext;
            _botService = botService;
        }

        public static bool GameExists(string gameName)
        {
            return _games.ContainsKey(gameName);
        }

        public static void CreateGameLock(string gameName)
        {
            _gameLocks[gameName] = new SemaphoreSlim(1, 1);
        }

        public static void DeleteGameLock(string gameName)
        {
            _gameLocks.TryRemove(gameName, out _);
        }

        public static SemaphoreSlim GetGameLock(string gameName)
        {
            if (!_gameLocks.ContainsKey(gameName)) 
                CreateGameLock(gameName);
            return _gameLocks[gameName];
        }

        public static void AddGameStates(List<Game> games)
        {
            foreach (Game game in games)
            {
                _games[game.GameName] = game;
                _gameLocks[game.GameName] = new SemaphoreSlim(1, 1);
            }
        }

        public async Task SaveGames()
        {
            if (_games.Count > 0)
            {
                List<Game> games = new List<Game>();

                foreach (Game game in _games.Values)
                {
                    SemaphoreSlim semaphore = _gameLocks[game.GameName];
                    await semaphore.WaitAsync();
                    games.Add(game);
                    semaphore.Release();
                }

                var options = new JsonSerializerOptions
                {
                    Converters = { new PlayerJsonConverter() },
                    WriteIndented = true
                };
                string json = JsonSerializer.Serialize(games, options);
                File.WriteAllText(MyInitializer.STORED_GAME_STATES_FILENAME, json);
            }

            _logger.LogInformation("Ongoing games saved.");

        }

        public Game? GetGameWithUser(User user)
        {
            return _games.Values.SingleOrDefault(game => game.Players.Any(player => player.Name == user.Username));
        }

        public async Task CloseGame(string gameName)
        {
            await closeGame(_games[gameName]);
        }

        public GetRunningGameModel GetRunningGame(User user)
        {
            Game? game = _games.Values.SingleOrDefault(g => g.Players.Any(p => p.Name == user.Username));

            if (game == null)
            {
                _logger.LogInformation("No running game found for {username}", user.Username);
                return GetRunningGameModel.NewNoResultInstance();
            }
            else
            {
                _logger.LogInformation("Returning running game {gameName} to {username}", game.GameName, user.Username);
                return GetRunningGameModel.NewResultInstance(game.GameName);
            }
        }

        public async Task<ClientGameState?> GetGameState(string gameName, User user)
        {
            if (!_games.ContainsKey(gameName))
            {
                _logger.LogWarning("{username} tried to get state for game {gameName} that does not exist",
                    user.Username, gameName);
                return null;
            }

            var gameLock = _gameLocks[gameName];
            await gameLock.WaitAsync();

            try
            {
                Game game = _games[gameName];

                if (!game.Players.Any(player => player.Name == user.Username))
                {
                    _logger.LogWarning("{username} tried to get state for game {gameName} but is not a player in that game",
                        user.Username, gameName);
                    return null;
                }

                ClientGameState gameState = new ClientGameState(user, game);
                await _hub.Clients.Group(gameName).SendAsync("playerConnected", user.Username);
                await _hub.Groups.AddToGroupAsync(_hub.Context.ConnectionId, gameName);
                _logger.LogInformation("returning state of game {gameName} to {username}", gameName, user.Username);
                return gameState;
            }
            finally
            {
                gameLock.Release();
            }

        }

        public async Task PlayCard(string gameName, Card card, User user)
        {
            if (!_games.ContainsKey(gameName))
            {
                _logger.LogWarning("{username} tried to play card in game {gameName}, but the game does not exist",
                    user.Username, gameName);
                return;
            }


            SemaphoreSlim semaphore = _gameLocks[gameName];
            await semaphore.WaitAsync();


            try
            {
                Game game = _games[gameName];

                if (game.Rounds.GetCurrentRound().Name == Rounds.Trumf && game.TrumpSuit == null)
                {
                    _logger.LogWarning("{username} tried to play card in Trump round before Trump suit is set. Game name: {gameName}",
                        user.Username, gameName);
                    return;
                }

                if (user.Username != game.NextToPlay)
                {
                    _logger.LogWarning("{username} tried to play a card in game {gameName} out of turn. Next to play = {nextToPlay}",
                        user.Username, gameName, game.NextToPlay);
                    return;
                }

                Player player = game.Players.Single(x => x.Name == user.Username);

                Card? cardInHand = player.Cards.SingleOrDefault(x => x == card);

                if (cardInHand is null)
                {
                    _logger.LogWarning("{username} tried to play a card he doesnt have in game {gameName}", user.Username, gameName);
                    return;
                }

                if (game.Rounds.GetCurrentRound().Name == Rounds.Kabal)
                {
                    await handleCardPlayedKabalRound(game, cardInHand, player);
                }
                else
                {
                    await handleCardPlayedTrickRound(game, cardInHand, player);
                }


            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Exception when {username} tried to play card in game {gameName}", user.Username, gameName);
            }
            finally
            {
                semaphore.Release();
            }
        }

        public async Task KabalPass(string gameName, User user)
        {
            if (!_games.ContainsKey(gameName))
            {
                _logger.LogWarning("{username} tried to cabal pass in a game that doesnt exist: {gameName}",
                    user.Username, gameName);
                return;
            }

            SemaphoreSlim semaphore = _gameLocks[gameName];
            await semaphore.WaitAsync();


            try
            {
                Game game = _games[gameName];
                if (game.Rounds.GetCurrentRound().Name != Rounds.Kabal)
                {
                    _logger.LogWarning("{username} tried to perform a cabal pass in round {roundName} of game {gameName}",
                        user.Username, game.Rounds.GetCurrentRound().Name, gameName);
                    return;
                }

                if (game.NextToPlay != user.Username)
                {
                    _logger.LogWarning("{username} tried to cabal pass in game {gameName} when it was {nextToPlay}'s turn",
                        user.Username, gameName, game.NextToPlay);
                    return;
                }

                game.RoundScores[Rounds.Kabal].AddPoints(user.Username, 1);
                game.MoveToNextPlayerClockwise();
                await _hub.Clients.Group(gameName).SendAsync("kabalPass", new KabalPassModel
                {
                    Passer = user.Username,
                    NextToPlay = game.NextToPlay
                });

                _ = _botService.PossiblyPerformBotActions_AfterCardPlayedOrPassByHuman_Kabal(game, semaphore, 
                    (lastCardWithOwner) => endKabalRound(game, lastCardWithOwner));
            }
            finally
            {
                semaphore.Release();
            }

            _logger.LogInformation("{username} performed cabal pass in game {gameName}", user.Username, gameName);
        }

        public async Task PerformTrumpSuitDraw(string gameName, int pickedIndex, User user)
        {
            if (!_games.ContainsKey(gameName))
            {
                _logger.LogWarning("{username} tried to perform trump suit draw in unexisting game {gameName}",
                    user.Username, gameName);
                return;
            }


            SemaphoreSlim semaphore = _gameLocks[gameName];
            await semaphore.WaitAsync();

            try
            {
                Game game = _games[gameName];
                if (game.Rounds.GetCurrentRound().Name != Rounds.Trumf)
                {
                    _logger.LogWarning("{username} tried to perform trump suit draw in round {roundName} of game {gameName}",
                        user.Username, game.Rounds.GetCurrentRound().Name, gameName);
                    return;
                }

                if (game.PlayerToPerformTrumpDraw != user.Username)
                {
                    _logger.LogWarning("{username} tried to perform trump suit draw when that honor belongs to {randomPlayer} in game {gameName}",
                        user.Username, game.PlayerToPerformTrumpDraw, gameName);
                    return;
                }

                if (game.TrumpSuit != null)
                {
                    _logger.LogWarning("{username} tried to perform trump suit draw twice in game {gameName}",
                        user.Username, gameName);
                    return;
                }

                game.PerformTrumpSuitDraw(pickedIndex);
                game.Rounds.GetCurrentRound().HasStarted = true;
                await _hub.Clients.Group(gameName).SendAsync("trumpSuitSelected", new TrumpSuitSelectedModel
                {
                    Suit = game.TrumpSuit,
                    PickedIndex = pickedIndex
                });

                _logger.LogInformation("{username} performed trump suit draw in game {gameName}. The trump is {trumpSuit}",
                    user.Username, gameName, game.TrumpSuit);
                _ = _botService.PossiblyPerformBotActions_AfterTrickEnded(game, semaphore);
            }
            finally
            {
                semaphore.Release();
            }
        }

        public async Task ClearOldGames(List<User> users)
        {
            foreach (User user in users)
            {
                var oldGames = _games.Values.Where(game => game.Players.Any(player => player.Name == user.Username));
                foreach (var game in oldGames)
                {
                    var gameLock = _gameLocks[game.GameName];
                    await gameLock.WaitAsync();
                    await closeGame(game);
                    gameLock.Release();
                    _gameLocks.TryRemove(game.GameName, out _);
                    _logger.LogInformation("Closed game {gameName} because because {username} is part of a new game that just started",
                        game.GameName, user.Username);
                }
            }
        }

        public Game CreateGame(GameSetup gameSetup)
        {
            var game = new Game(gameSetup);
            _games[gameSetup.GameName] = game;
            return game;
        }

        public Card? PerformBotAction(Game game, Player bot)
        {
            Card? played = _botService.GetBotMove(game.Rounds.GetCurrentRound().Name, (AiPlayer)bot, game);

            if (played == null)
                return null;

            game.CardsOnTable.Add(new CardWithOwner { Card = played, Owner = bot.Name });

            int valueToRemove = played.Value == 1 ? 14 : played.Value;
            bot.Cards.RemoveAll(x => x.Value == valueToRemove && x.Suit == played.Suit);
            game.MoveToNextPlayerClockwise();

            _logger.LogInformation("{suit}:{value} played by {username} in game {gameName}", played.Suit, played.Value, bot.Name, game.GameName);

            return played;
        }

        public List<string> GetActiveGames()
        {
            return _games.Values.Select(x => x.GameName).ToList();
        }

        private async Task closeGame(Game game)
        {
            await _hub.Clients.Group(game.GameName).SendAsync("gameClosed");

            var connections = _hub.GetConnections();

            foreach (var player in game.Players)
            {
                var userConnections = connections.Where(c => c.Value.Username == player.Name);
                foreach (var connection in userConnections)
                    await _hub.Groups.RemoveFromGroupAsync(connection.Key, game.GameName);
            }

            _games.TryRemove(game.GameName, out _);
            _logger.LogInformation("Game {gameName} closed", game.GameName);
        }

        private async Task handleCardPlayedKabalRound(Game game, Card card, Player player)
        {
            var kabalService = new KabalService();
            Card possiblyUpdatedCard; // if Ace used as One, value is changed to 1
            if (!kabalService.IsValidMove(card, game.CardsOnTable.Select(x => x.Card).ToList(), out possiblyUpdatedCard))
            {
                throw new Exception("PlayCard Kabal: " + player.Name + " tried to make an invalid move.");
            }

            var playedCardWithOwner = new CardWithOwner { Owner = player.Name, Card = possiblyUpdatedCard };
            game.CardsOnTable.Add(playedCardWithOwner);
            game.CardsOnTable = kabalService.RemoveUnneccesaryCard(possiblyUpdatedCard, game.CardsOnTable);
            player.Cards.Remove(card);

            if (player.Cards.Count == 0)
            {
                await endKabalRound(game, playedCardWithOwner);
            }
            else
            {
                game.MoveToNextPlayerClockwise();
                var cardPlayedModel = new CardPlayedModel
                {
                    Card = playedCardWithOwner,
                    NextToPlay = game.NextToPlay
                };
                await _hub.Clients.Group(game.GameName).SendAsync("cardPlayed", cardPlayedModel);

                _logger.LogInformation("{suit}:{value} played by {username} in game {gameName}", possiblyUpdatedCard.Suit, possiblyUpdatedCard.Value, player.Name, game.GameName);

                _ = _botService.PossiblyPerformBotActions_AfterCardPlayedOrPassByHuman_Kabal(game, _gameLocks[game.GameName],
                    (lastCardWithOwner) => endKabalRound(game, lastCardWithOwner));              
            }
        }


        private async Task endKabalRound(Game game, CardWithOwner playedCardWithOwner)
        {
            RoundScores kabalScores = game.GetCurrentRoundScores();
            foreach (var p in game.Players)
            {
                kabalScores.AddPoints(p.Name, p.Cards.Count);
            }

            if (game.GoToNextRound())
            {
                if (game.Rounds.GetCurrentRound().Name != Rounds.Trumf)
                    _botService.PossiblyPerformBotActions_NoDelay(game);

                var kabalEndedModel = new CardPlayedAndKabalEnded
                {
                    Card = playedCardWithOwner,
                };

                await _hubContext.Clients.Group(game.GameName).SendAsync("kabalCardPlayedAndRoundEnded", kabalEndedModel);
                _logger.LogInformation("{username} played the last card in Cabal round in game {gameName}. Moved to next round.",
                    playedCardWithOwner.Owner, game.GameName);
            }
            else
            {
                int gameId = await _scoreService.SaveScoresAndReturnGameId(game);
                deleteGame(game.GameName);

                var gameEndedModel = new GameEndedAfterKabalModel
                {
                    Card = playedCardWithOwner,
                    GameId = gameId
                };
                await _hubContext.Clients.Group(game.GameName).SendAsync("gameEndedAfterKabal", gameEndedModel);
                _logger.LogInformation("{username} played the last card in Cabal round in game {gameName}. That was the last round of the game.",
                    playedCardWithOwner.Owner, game.GameName);
            }
        }

        private async Task handleCardPlayedTrickRound(Game game, Card card, Player player)
        {
            var trickService = new TrickService(game.Rounds.GetCurrentRound().Name);
            bool isValidMove = trickService.IsValidMove(card, game.CardsOnTable.Select(x => x.Card).ToList(), player.Cards);
            if (!isValidMove)
                throw new Exception("Illegal move");

            var playedCardWithOwner = new CardWithOwner { Owner = player.Name, Card = card };
            game.CardsOnTable.Add(playedCardWithOwner);
            player.Cards.Remove(card);

            if (game.CardsOnTable.Count == game.Players.Count)
            {
                try
                {
                    await handleTrickEnded(game, player, trickService, playedCardWithOwner, _hubContext, _scoreService);
                }
                catch (Exception ex)
                {
                    _logger.LogWarning(ex.Message);
                    return;
                }
            }
            else
            {
                game.MoveToNextPlayerClockwise();
                await _hub.Clients.Group(game.GameName).SendAsync("cardPlayed",
                    new CardPlayedModel { Card = playedCardWithOwner, NextToPlay = game.NextToPlay });
                _logger.LogInformation("{suit}:{value} played by {username} in game {gameName}", card.Suit, card.Value, player.Name, game.GameName);

                _ = _botService.PossiblyPerformBotActions_AfterCardPlayedByHuman_TrickGame(game, _gameLocks[game.GameName],
                    (trickEnder, lastCardWithOwner, hubContext, scoreService) => handleTrickEnded(game, trickEnder, trickService, lastCardWithOwner, hubContext, scoreService));
            }
        }

        private async Task handleTrickEnded(Game game, Player player, TrickService trickService, CardWithOwner playedCardWithOwner, IHubContext<GameHub> hubContext, ScoreService scoreService)
        {
            if (game.Rounds.GetCurrentRound().Name == Rounds.Trumf) trickService.SetTrumpSuit(game.TrumpSuit);

            TrickResult trickResult = trickService.GetTrickResult(game.CardsOnTable);

            game.RoundScores[game.Rounds.GetCurrentRound().Name].AddPoints(trickResult.Winner, trickResult.PointsGained);

            if (player.Cards.Count == 0)
            {
                await endTrickRound(game, playedCardWithOwner, trickResult, false, scoreService);
            }

            else if (!hasPointLeftInRound(game))
            {
                await endTrickRound(game, playedCardWithOwner, trickResult, true, scoreService);
            }
            else
            {
                game.PreviousTrick = game.CardsOnTable.Select(x => x.Card).ToList();
                game.CardsOnTable.Clear();
                game.NextToPlay = game.Players.Single(x => x.Name == trickResult.Winner).Name;
                var trickEndedModel = new CardPlayedAndTrickEnded
                {
                    Card = playedCardWithOwner,
                    TrickResult = trickResult
                };
                await hubContext.Clients.Group(game.GameName).SendAsync("cardPlayedAndTrickEnded", trickEndedModel);
                _logger.LogInformation("{username} played the last card in a trick in game {gameName}", playedCardWithOwner.Owner, game.GameName);

                _ = _botService.PossiblyPerformBotActions_AfterTrickEnded(game, _gameLocks[game.GameName]);
            }

        }

        private async Task endTrickRound(Game game, CardWithOwner playedCardWithOwner, TrickResult trickResult, bool endedCuzNoPointCardsLeft, ScoreService scoreService)
        {
            _logger.LogInformation("Round Ended: {roundName} in game {gameName}", game.Rounds.GetCurrentRound().Name, game.GameName);

            if (game.GoToNextRound())
            {
                if (game.Rounds.GetCurrentRound().Name != Rounds.Trumf)
                    _botService.PossiblyPerformBotActions_NoDelay(game);

                var roundEndedModel = new TrickRoundEnded
                {
                    CardPlayed = playedCardWithOwner,
                    TrickResult = trickResult,
                    EndedCuzNoPointCardsLeft = endedCuzNoPointCardsLeft
                };
                await _hubContext.Clients.Group(game.GameName).SendAsync("cardPlayedAndRoundEnded", roundEndedModel);
                _logger.LogInformation("{username} played the last card of this trick round in game {gameName}", playedCardWithOwner.Owner, game.GameName);
            }
            else
            {
                int gameId = await scoreService.SaveScoresAndReturnGameId(game);
                deleteGame(game.GameName);
                var gameEndedModel = new GameEndedAfterTrickRoundModel
                {
                    CardPlayed = playedCardWithOwner,
                    TrickResult = trickResult,
                    GameId = gameId,
                    EndedCuzNoPointCardsLeft = endedCuzNoPointCardsLeft
                };
                await _hubContext.Clients.Group(game.GameName).SendAsync("gameEndedAfterTrickRound", gameEndedModel);
                _logger.LogInformation("{username} played the last card of this trick round, which was the last round of game {gameName}", playedCardWithOwner.Owner, game.GameName);
            }
        }

        private void deleteGame(string gameName)
        {
            _games.Remove(gameName, out _);
            _gameLocks.Remove(gameName, out _);
            _logger.LogInformation("Game {gameName} deleted", gameName);
        }

        private bool hasPointLeftInRound(Game game)
        {
            if (game.Rounds.GetCurrentRound().Name == Rounds.Spar)
            {

                foreach (var p in game.Players)
                {
                    if (CardService.ContainsSuit(p.Cards, Suits.Spades))
                        return true;
                }

                return false;
            }

            else if (game.Rounds.GetCurrentRound().Name == Rounds.Damer)
            {

                foreach (var p in game.Players)
                {
                    if (CardService.ContainsValue(p.Cards, 12))
                        return true;
                }

                return false;
            }
            else return true;
        }
    }
}
