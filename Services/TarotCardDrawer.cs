﻿using Kanin_Server.Models;
using Kanin_Server.Models.Entities;

namespace Kanin_Server.Services
{
    public class TarotCardDrawer
    {
        public const int TAROT_CARD_COUNT = 78;
        private readonly KaninContext _kaninContext;
        private Random random = new Random();

        public TarotCardDrawer(KaninContext kaninContext)
        {
            _kaninContext = kaninContext;
        }

        public TarotCard? draw(int chanceToOne)
        {
            int randomNum = random.Next(chanceToOne);

            if (randomNum != 0) return null;

            int randomId = random.Next(TAROT_CARD_COUNT) + 1;

            return _kaninContext.TarotCards.Single(x => x.Id == randomId);
        }
    }
}
