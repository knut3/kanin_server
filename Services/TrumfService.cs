﻿using Kanin_Server.Models;

namespace Kanin_Server.Services
{
    public class TrumfService
    {
        public TrickResult GetTrickResult(List<CardWithOwner> trick, string trumpSuit)
        {
            string leadingSuit = trick[0].Card.Suit;
            CardWithOwner? highestTrump = null;
            CardWithOwner? winner = trick[0];

            foreach (var card in trick)
            {
                if (card.Card.Value > winner.Card.Value && card.Card.Suit == leadingSuit)
                    winner = card;

                if (card.Card.Suit == trumpSuit)
                {
                    if (highestTrump == null)
                        highestTrump = card;
                    else if (card.Card.Value > highestTrump.Card.Value)
                        highestTrump = card;
                }
            }

            if (highestTrump != null) winner = highestTrump;

            return new TrickResult { Winner = winner.Owner, PointsGained = -1 };
        }
    }
}
