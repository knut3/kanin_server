﻿using Kanin_Server.Models;

namespace Kanin_Server.Services
{
    public class DamerService
    {
        public TrickResult GetTrickResult(List<CardWithOwner> trick)
        {
            string trickSuit = trick[0].Card.Suit;
            CardWithOwner winner = trick[0];
            int queens = 0;

            foreach (var card in trick)
            {
                if (card.Card.Value > winner.Card.Value && card.Card.Suit == trickSuit)
                    winner = card;

                if (card.Card.Value == CourtCards.Queen)
                    queens++;
            }
            int pointsGained = queens * 3;
            return new TrickResult { Winner = winner.Owner, PointsGained = pointsGained };
        }
    }
}
