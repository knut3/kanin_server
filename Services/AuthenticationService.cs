﻿using EasyEncryption;
using Kanin_Server.Models;

namespace Kanin_Server.Services
{
    public class AuthenticationService
    {
        private KaninContext _context;
        public AuthenticationService(KaninContext context)
        {
            _context = context;
        }
        public static string GetHash(string input)
        {
            return SHA.ComputeSHA256Hash(input);
        }

        public static string CreateAccessToken()
        {
            return Guid.NewGuid().ToString();
        }


    }
}
