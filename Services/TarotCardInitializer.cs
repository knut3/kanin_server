﻿namespace Kanin_Server.Models.Entities
{
    public static class TarotCardInitializer
    {
        private const string FolderName = "Tarot";
        private const string MajorFolderName = "Major";
        private const string MinorFolderName = "Minor";

        public static List<TarotCard> getTarotCards(string imagesPath)
        {
            string tarotPath = Path.Combine(imagesPath, "Tarot");
            var majorArcana = getMajorArcana(tarotPath);
            var minorArcana = getMinorArcana(tarotPath);
            return Enumerable.Concat(majorArcana, minorArcana).ToList();
        }

        private static List<TarotCard> getMajorArcana(string tarotPath)
        {
            List<TarotCard> result = new List<TarotCard>();
            string arcana = MajorFolderName;
            string path = tarotPath + @"/" + arcana;
            var cardFiles = new DirectoryInfo(path).GetFiles();
            int currentId = 1;
            foreach (var cardFile in cardFiles)
            {
                int value = int.Parse(Path.GetFileNameWithoutExtension(cardFile.Name));

                result.Add(new TarotCard
                {
                    Id = currentId,
                    Arcana = arcana,
                    Value = value,
                    Name = getMajorCardEnglishName(value),
                    RelativePath = getMajorPath(cardFile.Name)
                });

                currentId++;                
            }

            return result;
        }

        private static List<TarotCard> getMinorArcana(string tarotPath)
        {
            List<TarotCard> result = new List<TarotCard>();
            string arcana = MinorFolderName;
            string path = tarotPath + @"/" + arcana;
            var suitDirectories = new DirectoryInfo(path).GetDirectories();
            int currentId = 23;
            foreach (var suitDirectory in suitDirectories)
            {
                string suit = suitDirectory.Name;
                foreach (var file in suitDirectory.GetFiles())
                {
                    result.Add(new TarotCard
                    {
                        Id = currentId,
                        Arcana = arcana,
                        Suit = suit,
                        Value = int.Parse(Path.GetFileNameWithoutExtension(file.Name)),
                        RelativePath = getMinorPath(suit, file.Name)
                    });

                    currentId++;
                }
            }

            return result;
        }

        private static string getMinorPath(string suit, string fileName)
        {
            return @"StaticFiles/images/" + FolderName + @"/" + MinorFolderName + @"/" + suit + @"/" + fileName;
        }

        private static string getMajorPath(string fileName)
        {
            return @"StaticFiles/images/" + FolderName + @"/" + MajorFolderName + @"/" + fileName;
        }

        private static string getMajorCardEnglishName(int number)
        {
            switch (number)
            {
                case 0: return "The Fool";
                case 1: return "The Magician";
                case 2: return "The High Priestess";
                case 3: return "The Empress";
                case 4: return "The Emperor";
                case 5: return "The Hierophant";
                case 6: return "The Lovers";
                case 7: return "The Chariot";
                case 8: return "Strength";
                case 9: return "The Hermit";
                case 10: return "Wheel of Fortune";
                case 11: return "Justice";
                case 12: return "The Hanged Man";
                case 13: return "Death";
                case 14: return "Temperance";
                case 15: return "The Devil";
                case 16: return "The Tower";
                case 17: return "The Star";
                case 18: return "The Moon";
                case 19: return "The Sun";
                case 20: return "Judgement";
                default: return "The World";
            }
        }
    }
}
