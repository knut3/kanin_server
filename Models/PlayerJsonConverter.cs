﻿using System.Text.Json.Serialization;
using System.Text.Json;
using Kanin_Server.Models.Bots;

namespace Kanin_Server.Models
{
    public class PlayerJsonConverter : JsonConverter<Player>
    {
        private enum TypeDiscriminator
        {
            HumanPlayer = 0,
            AiPLayer = 1
        }

        public override bool CanConvert(Type type)
        {
            return typeof(Player).IsAssignableFrom(type);
        }

        public override Player Read(
            ref Utf8JsonReader reader,
            Type typeToConvert,
            JsonSerializerOptions options)
        {
            if (reader.TokenType != JsonTokenType.StartObject)
            {
                throw new JsonException();
            }

            if (!reader.Read()
                    || reader.TokenType != JsonTokenType.PropertyName
                    || reader.GetString() != "TypeDiscriminator")
            {
                throw new JsonException();
            }

            if (!reader.Read() || reader.TokenType != JsonTokenType.Number)
            {
                throw new JsonException();
            }

            Player player;
            TypeDiscriminator typeDiscriminator = (TypeDiscriminator)reader.GetInt32();
            switch (typeDiscriminator)
            {
                case TypeDiscriminator.HumanPlayer:
                    if (!reader.Read() || reader.GetString() != "TypeValue")
                    {
                        throw new JsonException();
                    }
                    if (!reader.Read() || reader.TokenType != JsonTokenType.StartObject)
                    {
                        throw new JsonException();
                    }
                    player = (HumanPlayer)JsonSerializer.Deserialize(ref reader, typeof(HumanPlayer));
                    break;
                case TypeDiscriminator.AiPLayer:
                    if (!reader.Read() || reader.GetString() != "TypeValue")
                    {
                        throw new JsonException();
                    }
                    if (!reader.Read() || reader.TokenType != JsonTokenType.StartObject)
                    {
                        throw new JsonException();
                    }
                    player = (AiPlayer)JsonSerializer.Deserialize(ref reader, typeof(AiPlayer));
                    break;
                default:
                    throw new NotSupportedException();
            }

            if (!reader.Read() || reader.TokenType != JsonTokenType.EndObject)
            {
                throw new JsonException();
            }

            return player;
        }

        public override void Write(
            Utf8JsonWriter writer,
            Player value,
            JsonSerializerOptions options)
        {
            writer.WriteStartObject();

            if (value is HumanPlayer humanPlayer)
            {
                writer.WriteNumber("TypeDiscriminator", (int)TypeDiscriminator.HumanPlayer);
                writer.WritePropertyName("TypeValue");
                JsonSerializer.Serialize(writer, humanPlayer);
            }
            else if (value is AiPlayer aiPlayer)
            {
                writer.WriteNumber("TypeDiscriminator", (int)TypeDiscriminator.AiPLayer);
                writer.WritePropertyName("TypeValue");
                JsonSerializer.Serialize(writer, aiPlayer);
            }
            else
            {
                throw new NotSupportedException();
            }

            writer.WriteEndObject();
        }
    }
}
