﻿namespace Kanin_Server.Models.Bots
{
    public class KnutsGrandPolicy : AiGainTricksPolicy
    {
        
        public KnutsGrandPolicy(int cardsPerPlayer) : base(cardsPerPlayer)
        {
        }

        public override Card? GetAction(List<Card> validMoves, Game game, List<Card> hand)
        {
            Card result;

            if (isTrickStarter(game))
            {
                result = getTrickStarterAction(game, hand);
            }
            else if (isTrickEnder(game))
            {
                result = getTrickEnderAction(game, validMoves);
            }
            else
            {
                result = getTrickMiddleAction(game, validMoves);
            }

            string trickSuit = game.CardsOnTable.Count > 0 ? game.CardsOnTable[0].Card.Suit : result.Suit;
            _tricksPerSuit[trickSuit]++;

            foreach (var card in game.CardsOnTable)
            {
                if (card.Card.Value > 9) _topCardsPlayed[card.Card.Suit].Add(card.Card);
            }

            if (result.Value > 9)
                _topCardsPlayed[result.Suit].Add(result);

            return result;
        }

        private Card getTrickStarterAction(Game game, List<Card> hand)
        {
            var suits = getSuits(hand);
            var longestSuit = suits.Aggregate((s1, s2) => s1.Value.Count + _tricksPerSuit[s1.Key] > s2.Value.Count + _tricksPerSuit[s2.Key] ? s1 : s2);

            if (longestSuit.Value.Count > 0)
            {
                int numOtherWinners = getNumWinners(suits, longestSuit.Key);
                return useLongestSuitPolicy(longestSuit, numOtherWinners, suits);
            }
            else
            {
                return playWinnerOrTryToMakeOne(suits);
            }

        }

        private Card playWinnerOrTryToMakeOne(IDictionary<string, List<Card>> suits)
        {
            var largestSuitOnHand = suits.Values.Aggregate((s1, s2) => s1.Count > s2.Count ? s1 : s2);

            if (largestSuitOnHand.Count == 1)
                return largestSuitOnHand[0];

            Card higest = getHighestCard(largestSuitOnHand);

            if (getNumWinners(largestSuitOnHand) > 0)
                return higest;

            var otherThanHighest = largestSuitOnHand.Where(x => x.Value != higest.Value).ToList();

            if (otherThanHighest.Count > 0)
                return otherThanHighest[new Random().Next(otherThanHighest.Count)];

            suits.Remove(higest.Suit);

            return playWinnerOrTryToMakeOne(suits);
            
        }

        private Card useLongestSuitPolicy(KeyValuePair<string, List<Card>> longestSuit, int numOtherWinners, IDictionary<string, List<Card>> otherSuits)
        {
            int estimatedRemainingCards = 13 - _tricksPerSuit[longestSuit.Key] * _numPlayers - longestSuit.Value.Count;
            int winnersNeeded = (int)Math.Ceiling(estimatedRemainingCards / (float)(_numPlayers - 1));
            int numWinners = getNumWinners(longestSuit.Value);
            int lossesNeededToClearSuit = winnersNeeded - numWinners;

            if (lossesNeededToClearSuit <= 0)
            {
                return getHighestCard(longestSuit.Value);
            }

            else if (numOtherWinners >= lossesNeededToClearSuit - 1)
            {
                return longestSuit.Value[new Random().Next(longestSuit.Value.Count)];
            }
            else if (numWinners > 0)
            {
                return getHighestCard(longestSuit.Value);
            }
            else
            {
                return playWinnerOrTryToMakeOne(otherSuits);
            }
        }

        private Card getTrickMiddleAction(Game game, List<Card> validMoves)
        {
            string trickSuit = game.CardsOnTable[0].Card.Suit;
            if (validMoves[0].Suit != trickSuit)
            {
                return getLowestCard(validMoves);
            }

            if (getNumWinners(validMoves) > 0)
                return getHighestCard(validMoves);

            Card trickLead = getHighestCard(game.CardsOnTable.Where(x => x.Card.Suit == trickSuit).Select(x => x.Card).ToList());
            Card? higherThanLead = getCardWithValueHigherThan(validMoves, trickLead.Value);

            if (higherThanLead != null)
                return higherThanLead;

            return getLowestCard(validMoves);
        }

        private Card getTrickEnderAction(Game game, List<Card> validMoves)
        {
            string trickSuit = game.CardsOnTable[0].Card.Suit;
            if (validMoves[0].Suit != trickSuit)
            {
                return getLowestCard(validMoves);
            }

            Card trickLead = getHighestCard(game.CardsOnTable.Where(x => x.Card.Suit == trickSuit).Select(x => x.Card).ToList());
            Card? higherThanLead = getCardWithValueHigherThan(validMoves, trickLead.Value);

            if (higherThanLead != null)
                return higherThanLead;

            return getLowestCard(validMoves);
        }

        private int getNumWinners(IDictionary<string, List<Card>> suits, string? excludeSuit = null)
        {
            int numWinners = 0;

            foreach (var suit in suits)
            {
                if (suit.Key == excludeSuit)
                    continue;

                numWinners += getNumWinners(suit.Value);
            }

            return numWinners;
        }
    }
}
