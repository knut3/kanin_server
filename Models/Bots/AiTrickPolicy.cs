﻿namespace Kanin_Server.Models.Bots
{
    public abstract class AiTrickPolicy : AiPolicy
    {
        protected IDictionary<string, int> _tricksPerSuit = new Dictionary<string, int>(4);
        protected readonly int _numPlayers;


        protected AiTrickPolicy(int cardsPerPlayer) : base(cardsPerPlayer)
        {
            foreach (var suit in Suits.Array)
                _tricksPerSuit[suit] = 0;

            _numPlayers = 52 / cardsPerPlayer;

        }

        protected bool isTrickStarter(Game game)
        {
            return game.CardsOnTable.Count == 0;
        }

        protected bool isTrickEnder(Game game)
        {
            return game.CardsOnTable.Count == game.Players.Count - 1;
        }
    }
}
