﻿namespace Kanin_Server.Models.Bots
{
    public class AiPlayer : Player
    {
        public string Intelligence { get; set; }
        private readonly IDictionary<string, AiPolicy> _roundPolicies;

        public AiPlayer()
        {

        }

        public AiPlayer(string name, string intelligence, int numPlayers) : base(name)
        {
            Intelligence = intelligence;
            _roundPolicies= new Dictionary<string, AiPolicy>();

            if (intelligence == BotModel.DUMB)
                initializeDumbPolicies();
            else if (intelligence == BotModel.MEDIUM)
                initilaizeMediumPolicies(numPlayers);
        }

        private void initilaizeMediumPolicies(int numPLayers)
        {
            int cardsPerPlayer = 52 / numPLayers;
            _roundPolicies.Add(Rounds.Pass, new KnutsPassPolicy(cardsPerPlayer));
            _roundPolicies.Add(Rounds.Spar, new KnutsSpadePolicy(cardsPerPlayer));
            _roundPolicies.Add(Rounds.Damer, new KnutsLadiesPolicy(cardsPerPlayer));
            _roundPolicies.Add(Rounds.Kabal, new KnutsKabalPolicy(cardsPerPlayer));
            _roundPolicies.Add(Rounds.Grand, new KnutsGrandPolicy(cardsPerPlayer));
            _roundPolicies.Add(Rounds.Trumf, new KnutsTrumpPolicy(cardsPerPlayer));
        }

        private void initializeDumbPolicies()
        {
            foreach (var roundName in Rounds.RoundNames)
            {
                _roundPolicies.Add(roundName, new RandomAiPolicy(-1));
            }
        }

        internal AiPolicy GetPolicy(string roundName)
        {
            return _roundPolicies[roundName];
        }
    }
}
