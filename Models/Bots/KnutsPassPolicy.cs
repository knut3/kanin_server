﻿using Kanin_Server.Services;
using System.Runtime.CompilerServices;

namespace Kanin_Server.Models.Bots
{
    public class KnutsPassPolicy : AiTrickPolicy
    {

        public KnutsPassPolicy(int cardsPerPlayer) : base(cardsPerPlayer)
        {
            
        }

        public override Card? GetAction(List<Card> validMoves, Game game, List<Card> hand)
        {
            Card result;

            if (isTrickStarter(game))
            {
                result = getTrickStarterAction(game, hand);
            }
            else if (isTrickEnder(game))
            {
                result = getTrickEnderAction(game, validMoves);
            }
            else
            {
                result = getTrickMiddleAction(game, validMoves);
            }

            string trickSuit = game.CardsOnTable.Count > 0 ? game.CardsOnTable[0].Card.Suit : result.Suit;
            _tricksPerSuit[trickSuit]++;

            return result;
        }

        private Card getTrickEnderAction(Game game, List<Card> validMoves)
        {
            string trickSuit = game.CardsOnTable[0].Card.Suit;
            bool hasTrickSuit = validMoves.Any(x => x.Suit == trickSuit);

            if (hasTrickSuit)
            {
                Card? lowerThanLead = getCardWithValueLowerThan(validMoves, game.CardsOnTable.Where(x => x.Card.Suit == trickSuit).Max(x => x.Card.Value));
                if (lowerThanLead != null) return lowerThanLead;

                else return getHighestCard(validMoves);
           
            }
            else
            {
                var suits = getSuits(validMoves).Where(x => x.Value.Count > 0).OrderBy(x => x.Value.Count);
                return getHighestCard(suits.First().Value);
            }

        }

        private Card getTrickMiddleAction(Game game, List<Card> validMoves)
        {
            string trickSuit = game.CardsOnTable[0].Card.Suit;
            bool hasTrickSuit = validMoves.Any(x => x.Suit == trickSuit);

            if (hasTrickSuit)
            {
                Card? lowerThanLead = getCardWithValueLowerThan(validMoves, game.CardsOnTable.Where(x => x.Card.Suit == trickSuit).Max(x => x.Card.Value));
                if (lowerThanLead != null) return lowerThanLead;

                if (new Random().NextDouble() < 0.8)
                {
                    return getHighestCard(validMoves);
                }
                else
                {
                    return validMoves[new Random().Next(validMoves.Count)];
                }
            }
            else
            {
                var suits = getSuits(validMoves).Where(x => x.Value.Count > 0).OrderBy(x => x.Value.Count);
                return getHighestCard(suits.First().Value);
            }
        }

            private Card getTrickStarterAction(Game game, List<Card> hand)
        {
            if (hand.Count < cardsPerPlayer / 4)
            {
                return getLowestCard(hand);
            }

            var suits = getSuits(hand).Where(x => x.Value.Count > 0).OrderBy(x => x.Value.Count);

            var smallestDeciredSuit = suits.Last().Value.OrderBy(x => x.Value).ToList(); 
            foreach (var suit in suits)
            {
                if (_tricksPerSuit[suit.Key] * _numPlayers + suit.Value.Count < cardsPerPlayer)
                {
                    smallestDeciredSuit = suit.Value.OrderBy(x => x.Value).ToList();
                    break;
                }
            }

            string suitOfSmallestDecired = smallestDeciredSuit.First().Suit;

            if (smallestDeciredSuit.Count + _tricksPerSuit[suitOfSmallestDecired] >= averageSuitCount)
            {
                return getLowestCard(hand);
            }

            if (smallestDeciredSuit.Median(x => x.Value) <= 7)
            {
                return smallestDeciredSuit.First();
            }
            else
            {
                return smallestDeciredSuit.Last();
            }
        }
    }
}
