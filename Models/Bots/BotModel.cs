﻿namespace Kanin_Server.Models.Bots
{
    public class BotModel
    {
        public const string DUMB = "dumb";
        public const string MEDIUM = "medium";


        public string Name { get; set; }

        public string Intelligence { get; set; }
    }
}
