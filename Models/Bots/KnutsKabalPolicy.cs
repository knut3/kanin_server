﻿using System.Collections.Generic;

namespace Kanin_Server.Models.Bots
{
    public class KnutsKabalPolicy : AiPolicy
    {
        private double openToCloseHolesEagerness;

        public KnutsKabalPolicy(int cardsPerPlayer) : base(cardsPerPlayer)
        {
            openToCloseHolesEagerness = new Random().NextDouble();
        }

        public override Card? GetAction(List<Card> validMoves, Game game, List<Card> hand)
        {
            var performances = getPerformanceInfos(validMoves, hand);

            var onlyOpeningWithReward = performances.Where(x => x.openingFactor == 0);

            if (onlyOpeningWithReward.Count() > 0)
            {
                var orderedByHoleSize = onlyOpeningWithReward.OrderBy(x => x.holeSize);
                if (new Random().NextDouble() <= openToCloseHolesEagerness)
                {
                    return orderedByHoleSize.Last().card;
                }
                else
                {
                    return orderedByHoleSize.First().card;
                }
            }
            else return performances.Aggregate((x1, x2) => x1.openingFactor < x2.openingFactor ? x1 : x2).card;
            
        }

        private List<PerformanceInfo> getPerformanceInfos(List<Card> validMoves, List<Card> hand)
        {
            var result = new List<PerformanceInfo>();
            var suits = getSuits(hand);

            foreach (var card in validMoves)
            {
                var suit = suits[card.Suit];
                Card? nextDown = getCardWithValueLowerThan(suit, card.Value);
                Card? nextUp = getCardWithValueHigherThan(suit, card.Value);

                var info = new PerformanceInfo();
                info.card = card;
                bool openTop = nextUp == null;
                bool openBottom = nextDown == null;
                info.openingFactor = getOpeningFactor(card, openTop, openBottom);
                info.holeSize = getHoleSize(card, nextDown, nextUp);

                result.Add(info);
            }

            return result;
        }

        private float getOpeningFactor(Card card, bool openTop, bool openBottom)
        {
            float aceValue = 0.5f;
            float openingFactor = 0;

            if (openTop)
            {
                openingFactor += CourtCards.King - card.Value + aceValue;
            }

            if (openBottom)
            {
                openingFactor += card.Value - aceValue;
            }

            return openingFactor;
        }

        private int getHoleSize(Card card, Card? nextDown, Card? nextUp)
        {
            if (card.Value < 7 && nextDown != null)
            {
                return card.Value - nextDown.Value - 1;
            }
            else if (card.Value > 8 && nextUp != null)
            {
                return nextUp.Value - card.Value - 1;
            }
            else if (card.Value == 8)
            {
                int result = 0;
                if (nextDown != null)
                    result += 6 - nextDown.Value - 1;
                if (nextUp != null)
                    result += nextUp.Value - 8 - 1;
                return result;
            }
            else if (card.Value == 7)
            {
                if (nextUp != null && nextUp.Value == 8)
                    return 0;

                int result = 0;
                if (nextDown != null)
                    result += 6 - nextDown.Value - 1;
                if (nextUp != null)
                    result += nextUp.Value - 7 - 1;
                return result;
            }

            return 0;
        }

        private class PerformanceInfo
        {
            public Card card;
            public float openingFactor;
            public int holeSize;
        }
    }
}
