﻿namespace Kanin_Server.Models.Bots
{
    public class KnutsSpadePolicy : AiTrickPolicy
    {
        public KnutsSpadePolicy(int cardsPerPlayer) : base(cardsPerPlayer)
        {
        }

        public override Card? GetAction(List<Card> validMoves, Game game, List<Card> hand)
        {
            Card result;

            if (isTrickStarter(game))
            {
                result = getTrickStarterAction(game, hand);
            }
            else if (isTrickEnder(game))
            {
                result = getTrickEnderAction(game, validMoves);
            }
            else
            {
                result = getTrickMiddleAction(game, validMoves);
            }

            string trickSuit = game.CardsOnTable.Count > 0 ? game.CardsOnTable[0].Card.Suit : result.Suit;
            _tricksPerSuit[trickSuit]++;

            return result;
        }

        private Card getTrickEnderAction(Game game, List<Card> validMoves)
        {
            string trickSuit = game.CardsOnTable[0].Card.Suit;
            bool hasTrickSuit = validMoves.Any(x => x.Suit == trickSuit);

            if (hasTrickSuit)
            {
                bool containsSpade = game.CardsOnTable.Any(x => x.Card.Suit == Suits.Spades);
                Card? lowerThanLead = getCardWithValueLowerThan(validMoves, 
                    game.CardsOnTable.Where(x => x.Card.Suit == trickSuit).Max(x => x.Card.Value));
                
                if (lowerThanLead != null && containsSpade) return lowerThanLead;

                else return getHighestCard(validMoves);

            }
            else
            {
                return getDiscardCardAction(validMoves);
            }

        }

        private Card getTrickMiddleAction(Game game, List<Card> validMoves)
        {
            string trickSuit = game.CardsOnTable[0].Card.Suit;
            bool hasTrickSuit = validMoves.Any(x => x.Suit == trickSuit);

            if (hasTrickSuit)
            {
                Card? lowerThanLead = getCardWithValueLowerThan(validMoves, game.CardsOnTable.Where(x => x.Card.Suit == trickSuit).Max(x => x.Card.Value));
                bool containsSpade = game.CardsOnTable.Any(x => x.Card.Suit == Suits.Spades);

                if (containsSpade)
                {
                    if (lowerThanLead != null)
                        return lowerThanLead;

                    else return getLowestCard(validMoves);
                }

                if (_tricksPerSuit[trickSuit] * _numPlayers + validMoves.Count < 13 - (_numPlayers - game.CardsOnTable.Count))
                {
                    return getHighestCard(validMoves);
                }

                if (lowerThanLead != null) return lowerThanLead;

                if (new Random().NextDouble() < 0.5)
                {
                    return getHighestCard(validMoves);
                }
                else
                {
                    return validMoves[new Random().Next(validMoves.Count)];
                }
            }
            else
            {
                return getDiscardCardAction(validMoves);
            }
        }

        private Card getTrickStarterAction(Game game, List<Card> hand)
        {
            var suits = getSuits(hand);
            var spades = suits[Suits.Spades].OrderBy(x => x.Value);
            var spadesBelow8 = spades.Where(x => x.Value < 8).ToList();
            var other = suits;
            other.Remove(Suits.Spades);
            other = other.OrderBy(x => x.Value.Count).Where(x => x.Value.Count > 0).ToDictionary(x => x.Key, x => x.Value);

            if (other.Count == 0)
            {
                return getLowestCard(hand);
            }

            if (spadesBelow8.Count > 0 && spadesBelow8.Count + _tricksPerSuit[Suits.Spades] > 8 / _numPlayers)
            {
                bool chooseLowSpade = new Random().Next(2) == 0;
                if (chooseLowSpade) return spadesBelow8[new Random().Next(spadesBelow8.Count)];
                else return getNoSpadeAction(other, spades.ToList());                         
            }
            else
            {
                return getNoSpadeAction(other, spades.ToList());
            }
        }

        private Card getNoSpadeAction(IDictionary<string, List<Card>> otherThanSpades, List<Card> spades)
        {
            var candidates = new List<Card>();
            var badCandidates = new List<Card>();

            foreach (var suit in otherThanSpades)
            {
                Card? candidate = null;

                if (suit.Value.Count + _tricksPerSuit[suit.Key] < averageSuitCount)
                {
                    candidate = getHighestCard(suit.Value);
                }
                else if (_tricksPerSuit[suit.Key] < averageSuitCount)
                {
                    candidate = suit.Value[new Random().Next(suit.Value.Count)];
                }
                else if (_tricksPerSuit[suit.Key] * _numPlayers + suit.Value.Count < 13)
                {
                    candidate = getLowestCard(suit.Value);
                }

                if (candidate != null)
                    candidates.Add(candidate);
                else
                    badCandidates.Add(getLowestCard(suit.Value));
            }

            if (candidates.Count > 0)
                return candidates[new Random().Next(candidates.Count)];

            if (spades.Count > 0)
                badCandidates.Add(getLowestCard(spades));

            return badCandidates[new Random().Next(badCandidates.Count)];
        }

        private Card getDiscardCardAction(List<Card> hand)
        {
            var suits = getSuits(hand);

            if (suits[Suits.Spades].Any(x => x.Value > 4))
                return getHighestCard(suits[Suits.Spades]);

            var others = suits.OrderBy(x => x.Value.Count).Where(x => x.Key != Suits.Spades && x.Value.Count > 0).ToDictionary(x => x.Key, x => x.Value);

            if (others.Count > 0)
                return getDiscardOthersAction(others);

            return getHighestCard(hand);
        }

        private Card getDiscardOthersAction(Dictionary<string, List<Card>> others)
        {
            foreach (var suit in others)
            {
                Card highest = getHighestCard(suit.Value);
                if (highest.Value > _numPlayers + 2)
                    return highest;
            }

            return getHighestCard(others.Values.First());
        }
    }
}
