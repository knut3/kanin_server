﻿namespace Kanin_Server.Models.Bots
{
    public class KnutsLadiesPolicy : AiTrickPolicy
    {
        private IDictionary<string, bool> queensGone = new Dictionary<string, bool>(4);

        public KnutsLadiesPolicy(int cardsPerPlayer) : base(cardsPerPlayer)
        {
            foreach (var suit in Suits.Array)
            {
                queensGone[suit] = false;
            }
        }

        public override Card? GetAction(List<Card> validMoves, Game game, List<Card> hand)
        {
            Card result;

            if (isTrickStarter(game))
            {
                result = getTrickStarterAction(game, hand);
            }
            else if (isTrickEnder(game))
            {
                result = getTrickEnderAction(game, validMoves);
            }
            else
            {
                result = getTrickMiddleAction(game, validMoves);
            }

            string trickSuit = game.CardsOnTable.Count > 0 ? game.CardsOnTable[0].Card.Suit : result.Suit;
            _tricksPerSuit[trickSuit]++;

            foreach (var card in game.CardsOnTable)
            {
                if (card.Card.Value == CourtCards.Queen) queensGone[card.Card.Suit] = true;
            }

            if (result.Value == CourtCards.Queen) queensGone[result.Suit] = true;


            return result;
        }

        private Card getTrickEnderAction(Game game, List<Card> validMoves)
        {
            string trickSuit = game.CardsOnTable[0].Card.Suit;
            bool hasTrickSuit = validMoves.Any(x => x.Suit == trickSuit);

            if (hasTrickSuit)
            {
                if (game.CardsOnTable.Any(x => x.Card.Suit == trickSuit && x.Card.Value > CourtCards.Queen))
                {
                    Card? queen = validMoves.FirstOrDefault(x => x.Value == CourtCards.Queen);
                    if (queen != null) return queen;
                }

                bool containsQueen = game.CardsOnTable.Any(x => x.Card.Value == CourtCards.Queen);
                Card? lowerThanLead = getCardWithValueLowerThan(validMoves, game.CardsOnTable.Where(x=>x.Card.Suit == trickSuit).Max(x => x.Card.Value));

                if (lowerThanLead != null && containsQueen) return lowerThanLead;

                Card highest = getHighestCard(validMoves);

                if (highest.Value != CourtCards.Queen) return highest;

                if (validMoves.Count() > 1)
                    return getCardWithValueLowerThan(validMoves, CourtCards.Queen);

                return highest;
            }
            else
            {
                return getDiscardCardAction(validMoves);
            }

        }

        private Card getTrickMiddleAction(Game game, List<Card> validMoves)
        {
            string trickSuit = game.CardsOnTable[0].Card.Suit;
            bool hasTrickSuit = validMoves.Any(x => x.Suit == trickSuit);

            if (hasTrickSuit)
            {
                var leadValue = game.CardsOnTable.Where(x => x.Card.Suit == trickSuit).Max(x => x.Card.Value);
                Card? lowerThanLead = getCardWithValueLowerThan(validMoves, leadValue);

                if (game.CardsOnTable.Any(x => x.Card.Suit == trickSuit && x.Card.Value > CourtCards.Queen))
                {
                    Card? queen = validMoves.FirstOrDefault(x => x.Value == CourtCards.Queen);
                    if (queen != null) return queen;
                    else if (lowerThanLead!= null) return lowerThanLead;
                    
                }

                bool otherQueenInPot = game.CardsOnTable.Any(x => x.Card.Value == CourtCards.Queen);
                
                if (queensGone[trickSuit])
                { 
                    if (lowerThanLead != null) return lowerThanLead;
                    if (otherQueenInPot) return getLowestCard(validMoves);
                    return validMoves[new Random().Next(validMoves.Count)];
                }
                else
                {
                    if (otherQueenInPot && lowerThanLead != null) return lowerThanLead;
                    else if (otherQueenInPot) return getLowestCard(validMoves);

                    Card? lowerThanQueen = getCardWithValueLowerThan(validMoves, CourtCards.Queen);
                    if (lowerThanQueen != null) return lowerThanQueen;
                    return getHighestCard(validMoves);
                }
            }
            else
            {
                return getDiscardCardAction(validMoves);
            }
        }

        private Card getTrickStarterAction(Game game, List<Card> hand)
        {
            var suits = getSuits(hand);
            var infos = getSuitInformation(suits);

            var priorityA = new List<Card>();
            var priorityB = new List<Card>();
            var priorityC = new List<Card>();

            foreach (var info in infos)
            {
                var suit = suits[info.Suit];

                if (suit.Count + _tricksPerSuit[info.Suit] * _numPlayers >= 13)
                {
                    priorityC.Add(getLowestCard(suit));
                    continue;
                }
                
                if (!info.ContainsQueen && info.NumHigherThanQueen == 0 && !queensGone[info.Suit])
                {
                    Card? card = getCardWithValueLowerThan(suit, CourtCards.Queen);
                    if (card != null) priorityA.Add(card);

                }

                if (info.NumBelowQueen + _tricksPerSuit[info.Suit] >= averageSuitCount && !queensGone[info.Suit])
                {
                    Card? card = getCardWithValueLowerThan(suit, CourtCards.Queen);
                    if (card != null) priorityB.Add(card);
                }

                if (info.NumBelowQueen > 0)
                {
                    priorityC.Add(getRandomBelowQueen(suit));
                }
            }

            Random random = new Random();

            if (priorityA.Count > 0)
                return priorityA[random.Next(priorityA.Count)];

            else if (priorityB.Count > 0)
                return priorityB[random.Next(priorityB.Count)];

            else if (priorityC.Count > 0)
                return priorityC[random.Next(priorityC.Count)];
            else
                return hand[random.Next(hand.Count)];


        }

        private Card getDiscardCardAction(List<Card> hand)
        {
            var suits = getSuits(hand).OrderByDescending(x => x.Value.Count);
            Card? queenSmallestSuit = null;
            Card? higherThanQueenSmallestSuit = null;
            Card highestCardSmallestSuit = null;

            foreach (var suit in suits)
            {
                var queen = suit.Value.SingleOrDefault(x => x.Value == CourtCards.Queen);
                if (queen != null) queenSmallestSuit = queen;
                var higherThanQueen = suit.Value.FirstOrDefault(x => x.Value > CourtCards.Queen);
                if (higherThanQueen != null) higherThanQueenSmallestSuit = higherThanQueen;
                if (suit.Value.Count > 0) highestCardSmallestSuit = getHighestCard(suit.Value);
            }

            if (queenSmallestSuit != null) return queenSmallestSuit;
            else if (higherThanQueenSmallestSuit != null) return higherThanQueenSmallestSuit;
            else return highestCardSmallestSuit;
        }

        private Card getRandomBelowQueen(List<Card> suit)
        {
            var below = suit.Where(x => x.Value < CourtCards.Queen).ToList();
            return below[new Random().Next(below.Count)];
        }

        private List<SuitInformation> getSuitInformation(IDictionary<string, List<Card>> suits)
        {
            var result = new List<SuitInformation>(4);

            foreach (var suit in suits)
            {
                if (suit.Value.Count == 0) continue;

                var info = new SuitInformation(suit.Key);

                foreach (var card in suit.Value)
                {
                    if (card.Value < CourtCards.Queen) info.NumBelowQueen++;
                    else if (card.Value == CourtCards.Queen) info.ContainsQueen = true;
                    else info.NumHigherThanQueen++;
                }

                result.Add(info);
            }
            return result;
        }

        private class SuitInformation
        {
            public SuitInformation(string suit)
            {
                Suit = suit;
            }

            public string Suit { get; set; }
            public bool ContainsQueen { get; set; } = false;
            public int NumHigherThanQueen { get; set; } = 0;

            public int NumBelowQueen { get; set; } = 0;
        }
    }
}