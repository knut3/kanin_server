﻿namespace Kanin_Server.Models.Bots
{
    public abstract class AiGainTricksPolicy : AiTrickPolicy
    {
        protected const int TOP_VALUE = 14;
        protected IDictionary<string, List<Card>> _topCardsPlayed = new Dictionary<string, List<Card>>(4);

        protected AiGainTricksPolicy(int cardsPerPlayer) : base(cardsPerPlayer)
        {
            foreach (var suit in Suits.Array)
            {
                _topCardsPlayed[suit] = new List<Card>();
            }
        }

        protected int getNumWinners(List<Card> suit)
        {
            int result = 0;

            if (suit.Count == 0) return 0;

            var topCardsPlayed = _topCardsPlayed[suit[0].Suit];

            for (int i = TOP_VALUE; i > 1; i--)
            {
                if (topCardsPlayed.Any(x => x.Value == i))
                    continue;
                else if (suit.Any(x => x.Value == i))
                    result++;
                else
                    return result;
            }

            return result;
        }
    }
}
