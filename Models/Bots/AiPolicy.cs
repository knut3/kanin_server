﻿namespace Kanin_Server.Models.Bots
{
    public abstract class AiPolicy
    {
        protected int cardsPerPlayer, averageSuitCount;

        protected AiPolicy(int cardsPerPlayer)
        {
            this.cardsPerPlayer = cardsPerPlayer;
            averageSuitCount = cardsPerPlayer / 4;
        }

        public abstract Card? GetAction(List<Card> validMoves, Game game, List<Card> hand);

        protected IDictionary<string, List<Card>> getSuits(List<Card> cards)
        {
            var result = new Dictionary<string, List<Card>>();

            foreach (string suit in Suits.Array)
            {
                result[suit] = cards.Where(x => x.Suit == suit).ToList();
            }

            return result;
        }

        protected Card? getCardWithValueLowerThan(List<Card> cards, int limit)
        {
            var ordered = cards.OrderByDescending(x => x.Value).ToList();

            foreach (var card in ordered)
            {
                if (card.Value < limit)
                    return card;
            }

            return null;
        }

        protected Card? getCardWithValueHigherThan(List<Card> cards, int limit)
        {
            var ordered = cards.OrderBy(x => x.Value).ToList();

            foreach (var card in ordered)
            {
                if (card.Value > limit)
                    return card;
            }

            return null;
        }

        protected Card getLowestCard(List<Card> cards)
        {
            return cards.Aggregate((c1, c2) => c1.Value < c2.Value ? c1 : c2);
        }

        protected Card getHighestCard(List<Card> cards)
        {
            return cards.Aggregate((c1, c2) => c1.Value > c2.Value ? c1 : c2);
        }

    }
}
