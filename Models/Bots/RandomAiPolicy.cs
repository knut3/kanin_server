﻿namespace Kanin_Server.Models.Bots
{
    public class RandomAiPolicy : AiPolicy
    {
        public RandomAiPolicy(int cardsPerPlayer) : base(cardsPerPlayer)
        {
        }

        public override Card? GetAction(List<Card> validMoves, Game game, List<Card> cards)
        {
            return validMoves[new Random().Next(validMoves.Count)];
        }
    }
}
