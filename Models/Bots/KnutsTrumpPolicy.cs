﻿namespace Kanin_Server.Models.Bots
{
    public class KnutsTrumpPolicy : AiGainTricksPolicy
    {
        public KnutsTrumpPolicy(int cardsPerPlayer) : base(cardsPerPlayer)
        {
        }

        public override Card? GetAction(List<Card> validMoves, Game game, List<Card> hand)
        {
            Card result;

            if (isTrickStarter(game))
            {
                result = getTrickStarterAction(game, hand);
            }
            else if (isTrickEnder(game))
            {
                result = getTrickEnderAction(game, validMoves);
            }
            else
            {
                result = getTrickMiddleAction(game, validMoves);
            }

            string trickSuit = game.CardsOnTable.Count > 0 ? game.CardsOnTable[0].Card.Suit : result.Suit;
            _tricksPerSuit[trickSuit]++;

            foreach (var card in game.CardsOnTable)
            {
                if (card.Card.Value > 9) _topCardsPlayed[card.Card.Suit].Add(card.Card);
            }

            if (result.Value > 9)
                _topCardsPlayed[result.Suit].Add(result);

            return result;
        }

        private Card getTrickMiddleAction(Game game, List<Card> validMoves)
        {
            string trickSuit = game.CardsOnTable[0].Card.Suit;
            bool isTrumpTrick = trickSuit == game.TrumpSuit;
            bool hasTrickSuit = validMoves[0].Suit == trickSuit;
            bool someoneHasTrumped = !isTrumpTrick && game.CardsOnTable.Any(x => x.Card.Suit == game.TrumpSuit);

            if (!hasTrickSuit)
            {
                var suits = getSuits(validMoves);

                if (suits[game.TrumpSuit].Count > 0)
                {
                    var trumpsOnTable = game.CardsOnTable.Where(x => x.Card.Suit == game.TrumpSuit).Select(x => x.Card).ToList();
                    if (trumpsOnTable.Count == 0)
                        return getLowestCard(suits[game.TrumpSuit]);

                    Card? higherThanHighestTrumpOnTable = getCardWithValueHigherThan(suits[game.TrumpSuit], getHighestCard(trumpsOnTable).Value);

                    if (higherThanHighestTrumpOnTable != null)
                        return higherThanHighestTrumpOnTable;
                    else
                    {
                        var other = validMoves.Where(x => x.Suit != game.TrumpSuit).ToList();
                        if (other.Count > 0)
                            return getLowestCard(other);
                    }

                }
                
                return getLowestCard(validMoves);
            }

            Card trickLead = getHighestCard(game.CardsOnTable.Where(x => x.Card.Suit == trickSuit).Select(x => x.Card).ToList());
            Card? higherThanLead = getCardWithValueHigherThan(validMoves, trickLead.Value);

            if (!someoneHasTrumped && higherThanLead != null)
                return new Random().Next(2) == 0 ? higherThanLead : getHighestCard(validMoves);
            else
                return getLowestCard(validMoves);
        }

        private Card getTrickEnderAction(Game game, List<Card> validMoves)
        {
            string trickSuit = game.CardsOnTable[0].Card.Suit;
            bool isTrumpTrick = trickSuit == game.TrumpSuit;
            bool hasTrickSuit = validMoves[0].Suit == trickSuit;
            bool someoneHasTrumped = !isTrumpTrick && game.CardsOnTable.Any(x => x.Card.Suit == game.TrumpSuit);

            if (!hasTrickSuit)
            {
                var suits = getSuits(validMoves);

                if (suits[game.TrumpSuit].Count > 0)
                {
                    var trumpsOnTable = game.CardsOnTable.Where(x => x.Card.Suit == game.TrumpSuit).Select(x => x.Card).ToList();
                    if (trumpsOnTable.Count == 0)
                        return getLowestCard(suits[game.TrumpSuit]);

                    Card? higherThanHighestTrumpOnTable = getCardWithValueHigherThan(suits[game.TrumpSuit], getHighestCard(trumpsOnTable).Value);

                    if (higherThanHighestTrumpOnTable != null)
                        return higherThanHighestTrumpOnTable;
                    else
                    {
                        var other = validMoves.Where(x => x.Suit != game.TrumpSuit).ToList();
                        if (other.Count > 0)
                            return getLowestCard(other);
                    }

                }

                return getLowestCard(validMoves);
            }
            
            Card trickLead = getHighestCard(game.CardsOnTable.Where(x => x.Card.Suit == trickSuit).Select(x => x.Card).ToList());
            Card? higherThanLead = getCardWithValueHigherThan(validMoves, trickLead.Value);

            if (!someoneHasTrumped && higherThanLead != null)
                return higherThanLead;
            else
                return getLowestCard(validMoves);
        }

        private Card getTrickStarterAction(Game game, List<Card> hand)
        {
            var otherSuits = getSuits(hand);
            var trumpSuit = otherSuits[game.TrumpSuit];
            otherSuits.Remove(game.TrumpSuit);
            otherSuits = otherSuits.OrderByDescending(x => x.Value.Count + _tricksPerSuit[x.Key])
                .Where(x => x.Value.Count > 0)
                .ToDictionary(x => x.Key, x => x.Value);

            if (otherSuits.Count == 0)
            {
                return getHighestCard(trumpSuit);
            }

            foreach (var suit in otherSuits)
            {
                if (getNumWinners(suit.Value) > 0)
                    return getHighestCard(suit.Value);

            }

            var shortestOtherShorterThanAverage = otherSuits.Last();

            if (shortestOtherShorterThanAverage.Value.Count + _tricksPerSuit[shortestOtherShorterThanAverage.Key] < averageSuitCount)
                return getLowestCard(shortestOtherShorterThanAverage.Value);

            if (getNumWinners(trumpSuit) > 0)
                return getHighestCard(trumpSuit);

            var largestSuitOtherThanTrump = otherSuits.First().Value;
            return largestSuitOtherThanTrump[new Random().Next(largestSuitOtherThanTrump.Count)];
        }

    }
}
