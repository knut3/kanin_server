﻿using Kanin_Server.Models.Bots;
using Kanin_Server.Models.Responses;
using Kanin_Server.Services;

namespace Kanin_Server.Models
{
    public class Game
    {
        public string GameName { get; set; }
        public User Creator { get; set; }
        public bool RandomPlayerToStartEachRound { get; set; }
        public List<Player> Players { get; set; }
        public Rounds Rounds { get; set; }
        public string StartedThisRound { get; set; }
        public string NextToPlay { get; set; }
        public List<CardWithOwner> CardsOnTable { get; set; }
        public List<Card>? PreviousTrick { get; set; }
        public string? PlayerToPerformTrumpDraw { get; set; }
        public string? TrumpSuit { get; set; }
        public Dictionary<string, RoundScores> RoundScores { get; set; }

        public Game(GameSetup setup)
        {
            GameName = setup.GameName;
            Creator = setup.Creator;
            Players = new List<Player>();
            setup.Users.ForEach(x => Players.Add(new HumanPlayer(x.Username)));
            int numPlayers = setup.Users.Count + setup.Bots.Count;
            setup.Bots.ForEach(x => Players.Add(new AiPlayer(x.Name, x.Intelligence, numPlayers)));
            randomizeSeatingOrder();
            Rounds = new Rounds(false);
            RandomPlayerToStartEachRound = setup.RandomPlayerToStartEachRound;
            NextToPlay = selectRandomPlayer();
            StartedThisRound = NextToPlay;
            CardService.Deal(Players);
            Players.ForEach(x => x.Cards = CardService.Sort(x.Cards));
            CardsOnTable = new List<CardWithOwner>();
            PreviousTrick = null;
            TrumpSuit = null;
            initializeRoundScores();
            if (Rounds.GetCurrentRound().Name == Rounds.Kabal)
            {
                NextToPlay = getPlayerWithSevenOfClubsOnHand();
            }
            else if (Rounds.GetCurrentRound().Name == Rounds.Trumf)
            {
                drawHumanPlayerToPerformTrumpDraw();
            }

        }

        public Game()
        {
    
        }

        public void MoveToNextPlayerClockwise()
        {
            NextToPlay = getPlayerToTheLeftOf(NextToPlay);
        }

        public RoundScores GetCurrentRoundScores()
        {
            string roundName = Rounds.GetCurrentRound().Name;
            return RoundScores[roundName];
        }

        public RoundScores GetPreviousRoundScores()
        {
            string roundName = Rounds.GetPreviousRound().Name;
            return RoundScores[roundName];
        }

        public List<ScoreModel> GetTotalScores()
        {
            var totalScores = new Dictionary<string, ScoreModel>();
            foreach (var roundScores in RoundScores)
            {
                foreach (var playerScore in roundScores.Value.Scores)
                {
                    string username = playerScore.Key;
                    if (!totalScores.ContainsKey(username))
                    {
                        totalScores.Add(username, new ScoreModel { Username = username, Score = playerScore.Value });
                    }
                    else
                    {
                        totalScores[username].Score += playerScore.Value;
                    }
                }
            }

            return totalScores.Values.ToList();
        }

        public bool GoToNextRound()
        {
            if (Rounds.IsLastRound()) return false;

            Rounds.FinishRound();

            StartedThisRound = NextToPlay = getPlayerToTheLeftOf(StartedThisRound);

            CardsOnTable.Clear();
            CardService.Deal(Players);
            Players.ForEach(p => p.Cards = CardService.Sort(p.Cards));
            PreviousTrick = null;
            TrumpSuit= null;

            if (Rounds.GetCurrentRound().Name == Rounds.Kabal)
            {
                NextToPlay = getPlayerWithSevenOfClubsOnHand();
            }
            else if (Rounds.GetCurrentRound().Name == Rounds.Trumf)
            {
                drawHumanPlayerToPerformTrumpDraw();
            }

            return true;
        }

        private void drawHumanPlayerToPerformTrumpDraw()
        {
            var humanPlayers = new List<Player>();
            foreach (var player in Players)
            {
                if (player is HumanPlayer) 
                    humanPlayers.Add(player);
            }

            PlayerToPerformTrumpDraw = humanPlayers[new Random().Next(humanPlayers.Count)].Name;
        }

        public void PerformTrumpSuitDraw(int pickedIndex)
        {
            int randomAddition = new Random().Next(4);
            int resultIndex = (pickedIndex + randomAddition) % 4;
            TrumpSuit = Suits.Array[resultIndex];
        }

        private void randomizeSeatingOrder()
        {
            List<Player> result = new List<Player>(Players.Count);

            Random random = new Random();
            while (Players.Count > 0)
            {
                int randomIndex = random.Next(Players.Count);
                result.Add(Players[randomIndex]);
                Players.RemoveAt(randomIndex);
            }

            Players = result;
        }

        private string selectRandomPlayer()
        {
            int randomIndex = new Random().Next(0, Players.Count);
            Player player = Players[randomIndex];           
            return player.Name;
        }

        private string getPlayerToTheLeftOf(string username)
        {
            Player currentPlayer = Players.Single(x => x.Name == username);

            int i = Players.IndexOf(currentPlayer);
            i++;
            if (i == Players.Count) i = 0;

            return Players.ElementAt(i).Name;
        }

        private void initializeRoundScores()
        {
            RoundScores = new Dictionary<string, RoundScores>();
            string[] usernames = Players.Select(x => x.Name).ToArray();
            foreach (string roundName in Rounds.RoundNames)
            {
                RoundScores.Add(roundName, new RoundScores(roundName, usernames));
            }
        }

        private string getPlayerWithSevenOfClubsOnHand()
        {
            Card sevenOfClubs = new Card(Suits.Clubs, 7);
            return Players.Single(p => p.Cards.Contains(sevenOfClubs)).Name;
        }
    }
}
