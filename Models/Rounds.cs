﻿using System.Text.Json.Serialization;

namespace Kanin_Server.Models
{
    public class Rounds
    {
        public const string Pass = "Pass";
        public const string Spar = "Spar";
        public const string Damer = "Damer";
        public const string Kabal = "Kabal";
        public const string Grand = "Grand";
        public const string Trumf = "Trumf";
        public static readonly string[] RoundNames = { Pass, Spar, Damer, Kabal, Grand, Trumf };


        public Round[] rounds { get; set; } = new Round[RoundNames.Length];
        public int currentRoundIndex { get; set; }

        public Rounds(bool randomOrder)
        {
            if (randomOrder) initializeRoundsRandomally();
            else initializeRoundsInNormalOrder();

            currentRoundIndex = 0;
        }

        public Rounds()
        {

        }

        public Round GetCurrentRound()
        {
            return rounds[currentRoundIndex];
        }

        public Round GetPreviousRound()
        {
            if (currentRoundIndex == 0)
            {
                Console.WriteLine("Trying to get previous round when in First round");
                return null;
            }

            return rounds[currentRoundIndex - 1];
        }

        public void FinishRound()
        {
            currentRoundIndex++;
        }

        public bool IsLastRound()
        {
            return (currentRoundIndex == rounds.Length - 1);
        }

        private void initializeRoundsRandomally()
        {
            List<int> availableIndecies = new List<int> { 0, 1, 2, 3, 4, 5 };
            Random random = new Random();
            while (availableIndecies.Count > 0)
            {
                int randomIndex = random.Next(availableIndecies.Count);
                string roundName = RoundNames[availableIndecies[randomIndex]];
                int roundNumber = availableIndecies[randomIndex] + 1;
                rounds[randomIndex] = new Round(roundName, roundNumber);
                availableIndecies.RemoveAt(randomIndex);
            }
        }

        private void initializeRoundsInNormalOrder()
        {
            for (int i = 0; i < rounds.Length; i++)
            {
                string roundName = RoundNames[i];
                int roundNumber = i + 1;
                rounds[i] = new Round(roundName, roundNumber);
            }
        }
    }
}
