﻿namespace Kanin_Server.Models
{
    public static class Suits
    {
        public static readonly string Clubs = "clubs";
        public static readonly string Hearts = "hearts";
        public static readonly string Diamonds = "diamonds";
        public static readonly string Spades = "spades";

        public static readonly string[] Array = { Clubs, Hearts, Diamonds, Spades };
    }
}
