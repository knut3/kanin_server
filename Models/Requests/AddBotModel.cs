﻿namespace Kanin_Server.Models.Requests
{
    public class AddBotModel
    {
        public string GameName { get; set; }
        public string Intelligence{ get; set; }
    }
}
