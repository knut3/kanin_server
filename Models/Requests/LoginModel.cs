﻿namespace Kanin_Server.Models.Input
{
    public class LoginModel
    {
        public String UserName { get; set; }
        public String Password { get; set; }
    }
}
