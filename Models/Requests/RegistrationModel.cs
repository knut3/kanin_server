﻿using System.ComponentModel.DataAnnotations;

namespace Kanin_Server.Models.Input
{
    public class RegistrationModel
    {
        [Required]
        [StringLength(11, MinimumLength = 1)]
        public String UserName { get; set; }
        [Required]
        [StringLength(30, MinimumLength = 1)]
        public String Password { get; set; }
    }
}
