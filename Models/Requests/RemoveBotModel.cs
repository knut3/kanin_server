﻿namespace Kanin_Server.Models.Requests
{
    public class RemoveBotModel
    {
        public string GameName { get; set; }
        public string BotName { get; set; }
    }
}