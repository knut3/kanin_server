﻿using Kanin_Server.Models.Input;

namespace Kanin_Server.Models.Requests
{
    public class JoinLobbyModel
    {
        public string GameName { get; set; }
    }
}
