﻿namespace Kanin_Server.Models
{
    public class TrickResult
    {
        public string Winner { get; set; }
        public int PointsGained { get; set; }
    }
}
