﻿namespace Kanin_Server.Models
{
    public class Round
    {
        public string Name { get; set; }
        public int Number { get; set; }
        public bool HasStarted { get; set; }

        public Round(string name, int number)
        {
            Name = name;
            Number = number;
            HasStarted = false;
        }
    }
}
