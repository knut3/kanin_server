﻿namespace Kanin_Server.Models
{
    public abstract class Player
    {
        public string Name { get; set; }
        public List<Card> Cards { get; set; }

        public Player(string name)
        {
            Cards = new List<Card>();
            Name = name;
        }

        public Player()
        {

        }
    }
}
