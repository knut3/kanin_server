﻿namespace Kanin_Server.Models
{
    public class Card
    {
        public string Suit { get; set; }
        public int Value { get; set; }

        public Card(string suit, int value)
        {
            Suit = suit;
            Value = value;
        }

        public bool Equals(Card? other)
        {
            if (other is null) return false;
            if (ReferenceEquals(this, other)) return true;
            return this.Suit.Equals(other.Suit) && this.Value.Equals(other.Value);
        }

        public static bool operator ==(Card? lhs, Card? rhs)
        {
            if (lhs is null)
            {
                if (rhs is null)
                {
                    return true;
                }

                // Only the left side is null.
                return false;
            }
            // Equals handles case of null on right side.
            return lhs.Equals(rhs);
        }

        public static bool operator !=(Card? lhs, Card? rhs) => !(lhs == rhs);

        public override bool Equals(object? obj)
        {
            return Equals(obj as Card);
        }

        public override int GetHashCode()
        {
            return (Suit, Value).GetHashCode();
        }
    }
}
