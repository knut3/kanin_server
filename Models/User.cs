﻿using Kanin_Server.Models.Entities;

namespace Kanin_Server.Models
{
    public class User : IEquatable<User>
    {
        public string Username { get; set; }
        public bool IsAdmin { get; set; }

        public User(UserLogin userLogin)
        {
            this.Username = userLogin.Name;
            this.IsAdmin = userLogin.IsAdmin;
        }

        public User()
        {

        }

        public bool Equals(User? other)
        {
            if (other is null) return false;
            if (ReferenceEquals(this, other)) return true;
            return this.Username.Equals(other.Username);
        }

        public static bool operator ==(User? lhs, User? rhs)
        {
            if (lhs is null)
            {
                if (rhs is null)
                {
                    return true;
                }

                // Only the left side is null.
                return false;
            }
            // Equals handles case of null on right side.
            return lhs.Equals(rhs);
        }

        public static bool operator !=(User? lhs, User? rhs) => !(lhs == rhs);

        public override bool Equals(object? obj)
        {
            return Equals(obj as User);
        }

        public override int GetHashCode()
        {
            return Username.GetHashCode();
        }
    }
}
