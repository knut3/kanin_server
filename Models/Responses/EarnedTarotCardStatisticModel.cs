﻿using Kanin_Server.Models.Entities;

namespace Kanin_Server.Models.Responses
{
    public class EarnedTarotCardStatisticModel
    {
        public TarotCard Card { get; set; }
        public int ReasonWinnerCount { get; set; }
        public int ReasonRabbitCount { get; set; }
    }
}
