﻿namespace Kanin_Server.Models.Responses
{
    public class TrumpSuitSelectedModel
    {
        public string Suit { get; set; }
        public int PickedIndex { get; set; }

    }
}
