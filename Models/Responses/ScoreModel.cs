﻿namespace Kanin_Server.Models.Responses
{
    public class ScoreModel
    {
        public string Username { get; set; }
        public int Score { get; set; }

    }
}
