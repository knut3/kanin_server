﻿namespace Kanin_Server.Models.Responses
{
    public class Opponent
    {
        public string Username { get; set; }
        public int NumCardsOnHand { get; set; }

        public Opponent(Player player)
        {
            Username = player.Name;
            NumCardsOnHand = player.Cards.Count;
        }
    }
}
