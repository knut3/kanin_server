﻿namespace Kanin_Server.Models.Responses
{
    public class CardPlayedAndTrickEnded
    {
        public CardWithOwner Card { get; set; }
        public TrickResult TrickResult { get; set; }
    }
}
