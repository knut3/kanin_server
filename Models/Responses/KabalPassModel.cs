﻿namespace Kanin_Server.Models.Responses
{
    public class KabalPassModel
    {
        public string Passer { get; set; }
        public string NextToPlay { get; set; }

    }
}
