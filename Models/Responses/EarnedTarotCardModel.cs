﻿using Kanin_Server.Models.Entities;

namespace Kanin_Server.Models.Responses
{
    public class EarnedTarotCardModel
    {
        public TarotCard Card { get; set; }
        public string Reason { get; set; }
    }
}
