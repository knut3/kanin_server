﻿using Kanin_Server.Models;

namespace Kanin_Server.Models.Responses
{
    public class ClientGameState
    {
        public Round Round { get; set; }
        public Opponent[] Opponents { get; set; }
        public List<Card> YourCards { get; set; }
        public string NextToPlay { get; set; }
        public List<CardWithOwner> CardsOnTable { get; set; }
        public List<Card>? PreviousTrick { get; set; }
        public string? PlayerToPerformTrumpDraw { get; set; }
        public string? TrumpSuit { get; set; }
        public List<PlayerScore> Scores { get; set; }

        public ClientGameState(User you, Game game)
        {
            Round = game.Rounds.GetCurrentRound();
            Opponents = getOpponentsInCorrectOrder(game.Players, you);
            YourCards = game.Players.Single(x => x.Name == you.Username).Cards;
            NextToPlay = game.NextToPlay;
            CardsOnTable = game.CardsOnTable;
            PreviousTrick = game.PreviousTrick;
            PlayerToPerformTrumpDraw = game.PlayerToPerformTrumpDraw;
            TrumpSuit = game.TrumpSuit;
            var roundScores = game.RoundScores[Round.Name];
            var totalScores = game.GetTotalScores();
            Scores = new List<PlayerScore>(game.Players.Count);
            foreach (var total in totalScores)
            {
                var playerScore = new PlayerScore
                {
                    Username = total.Username,
                    TotalScore = total.Score,
                    RoundScore = roundScores.Scores[total.Username]
                };

                Scores.Add(playerScore);

            }
        }

        private Opponent[] getOpponentsInCorrectOrder(List<Player> players, User you)
        {
            Player yourPlayer = players.Single(x => x.Name == you.Username);
            int yourIndex = players.IndexOf(yourPlayer);
            int numOpponents = players.Count - 1;

            Opponent[] opponents = new Opponent[numOpponents];
            for (int i = 0; i < numOpponents; i++)
            {
                int nextOpponentIndex = (yourIndex + 1 + i) % players.Count;
                opponents[i] = new Opponent(players[nextOpponentIndex]);
            }

            return opponents;
        }
    }
}
