﻿namespace Kanin_Server.Models.Responses
{
    public class CardPlayedAndKabalEnded
    {
        public CardWithOwner Card { get; set; }

        public bool GameFinished { get; set; }
    }
}
