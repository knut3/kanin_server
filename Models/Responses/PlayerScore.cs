﻿using System;

public class PlayerScore
{
    public string Username { get; set; }
    public int RoundScore { get; set; }
    public int TotalScore { get; set; }

}
