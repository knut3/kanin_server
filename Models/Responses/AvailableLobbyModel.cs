﻿namespace Kanin_Server.Models.Responses
{
    public class AvailableLobbyModel
    {
        public string Name { get; set; }
        public int PlayerCount { get; set; }
    }
}
