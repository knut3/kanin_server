﻿namespace Kanin_Server.Models.Responses
{
    public class GetOpenLobbyModel
    {
        public bool FoundResult { get; set; }
        public string? GameName { get; set; }
        public bool IsCreator { get; set; } = false;
    }
}
