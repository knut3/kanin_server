﻿namespace Kanin_Server.Models.Responses
{
    public class CardPlayedModel
    {
        public CardWithOwner Card { get; set; }
        public string NextToPlay { get; set; }
    }
}
