﻿namespace Kanin_Server.Models.Responses
{
    public class GetRunningGameModel
    {
        public bool FoundResult { get; set; }
        public string? GameName { get; set; }

        public static GetRunningGameModel NewNoResultInstance()
        {
            return new GetRunningGameModel { FoundResult = false };
        }

        public static GetRunningGameModel NewResultInstance(string gameName)
        {
            return new GetRunningGameModel { FoundResult = true, GameName = gameName };
        }
    }
}
