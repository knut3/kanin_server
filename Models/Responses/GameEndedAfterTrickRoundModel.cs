﻿namespace Kanin_Server.Models.Responses
{
    public class GameEndedAfterTrickRoundModel : TrickRoundEnded
    {
        public int GameId { get; set; }
    }
}
