﻿using Kanin_Server.Models.Entities;

namespace Kanin_Server.Models.Responses
{
    public class GameResultModel
    {
        public List<ScoreModel> TotalScores { get; set; }
        public EarnedTarotCardModel? EarnedTarotCard { get; set; } // for temporary backward comp
        public List<EarnedTarotCardModel> EarnedTarotCards { get; set; }

        public void SetEarnedTarotCards(List<EarnedTarotCard> entities)
        {
            EarnedTarotCards = entities.Select(x => new EarnedTarotCardModel
            {
                Card = x.Card,
                Reason = x.Reason
            }).ToList();

            if (EarnedTarotCards.Count > 0)
                EarnedTarotCard = EarnedTarotCards.First();
        }
    }
}
