﻿namespace Kanin_Server.Models.Responses
{
    public class GameEndedAfterKabalModel : CardPlayedAndKabalEnded
    {
        public int GameId { get; set; }
    }
}
