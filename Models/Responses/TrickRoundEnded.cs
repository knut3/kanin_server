﻿namespace Kanin_Server.Models.Responses
{
    public class TrickRoundEnded
    {
        public CardWithOwner CardPlayed { get; set; }
        public TrickResult TrickResult { get; set; }
        public bool EndedCuzNoPointCardsLeft { get; set; } = false;
    }
}
