﻿using Kanin_Server.Models.Bots;

namespace Kanin_Server.Models
{
    public class GameSetup
    {
        public string GameName { get; set; }
        public User Creator { get; set; }
        public List<User> Users { get; set; }

        public List<BotModel> Bots { get; set; }

        public bool RandomPlayerToStartEachRound { get; set; }

        public GameSetup()
        {
            Users = new List<User>();
            Bots = new List<BotModel>();
            RandomPlayerToStartEachRound = false;
        }

        public int GetPlayersCount()
        {
            return Users.Count + Bots.Count;
        }

        public ForClient ToClientModel()
        {
            return new ForClient
            {
                GameName = GameName,

                Usernames = Users.Select(x => x.Username).ToArray(),

                Bots = Bots.ToArray()
        };
        }

        public static ForClient ToClientModelGameHasStarted(string gameName)
        {
            return new ForClient 
            {
                GameHasStarted = true,
                GameName = gameName
            };
        }

        public class ForClient
        {
            public bool GameHasStarted { get; set; } = false;
            public string GameName { get; set; }
            public string[]? Usernames { get; set; }
            public BotModel[]? Bots { get; set; }
        }
    }
}
