﻿namespace Kanin_Server.Models
{
    public class RoundScores
    {
        public string RoundName { get; set; }
        public Dictionary<string, int> Scores { get; set; }

        public RoundScores(string roundName, string[] usernames)
        {
            RoundName = roundName;
            Scores = new Dictionary<string, int>();
            foreach (var user in usernames)
                Scores[user] = 0;
        }

        public RoundScores()
        {

        }

        public void AddPoints(string username, int points)
        {
            Scores[username] += points;
        }
    }
}
