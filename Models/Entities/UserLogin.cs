﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Kanin_Server.Models.Entities
{
    public class UserLogin
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [MaxLength(255)]
        public string Name { get; set; }
        [MaxLength(255)]
        public string PasswordHash { get; set; }
        [MaxLength(255)]
        public string AccessToken { get; set; }
        public bool IsAdmin { get; set; } = false;

    }
}
