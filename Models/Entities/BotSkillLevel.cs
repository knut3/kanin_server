﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Kanin_Server.Models.Entities
{
    public class BotSkillLevel
    {
        public int Id { get; set; }
        public int GameId { get; set; }
        [MaxLength(255)]
        public string BotName { get; set; }
        [MaxLength(255)]
        public string SkillLevel { get; set; }
    }
}
