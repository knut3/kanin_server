﻿using System.ComponentModel.DataAnnotations;

namespace Kanin_Server.Models.Entities
{
    public class EarnedTarotCard
    {
        public int Id { get; set; }
        [MaxLength(255)]
        public string Username { get; set; }
        public int GameId { get; set; }
        [MaxLength(255)]
        public string Reason { get; set; }

        public virtual TarotCard Card { get; set; }

    }
}
