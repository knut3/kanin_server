﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Kanin_Server.Models.Entities
{
    public class TarotCard
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Id { get; set; }
        public string RelativePath { get; set; }
        [MaxLength(255)]
        public string Arcana { get; set; }
        [MaxLength(255)]
        public string? Name { get; set; } = null;
        [MaxLength(255)]
        public string? Suit { get; set; } = null;
        public int Value { get; set; }

    }
}
