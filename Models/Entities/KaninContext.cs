﻿
using Kanin_Server.Models.Entities;
using Kanin_Server.Services;
using Microsoft.EntityFrameworkCore;

namespace Kanin_Server.Models
{
    public class KaninContext : DbContext
    {
        private IWebHostEnvironment _webHostEnvironment;
        public KaninContext(DbContextOptions<KaninContext> options, IWebHostEnvironment webHostEnvironment)
            : base(options)
        {
            _webHostEnvironment = webHostEnvironment;
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            string imagesPath = Path.Combine(_webHostEnvironment.ContentRootPath, @"StaticFiles/images");
            modelBuilder.Entity<TarotCard>().HasData(TarotCardInitializer.getTarotCards(imagesPath));
        }

        public DbSet<UserLogin> Users { get; set; }
        public DbSet<GameEntity> Games { get; set; }
        public DbSet<RoundScore> RoundScores { get; set; }
        public DbSet<TarotCard> TarotCards { get; set; }
        public DbSet<EarnedTarotCard> EarnedTarotCards { get; set; }
        public DbSet<BotSkillLevel> BotSkillLevels { get; set; }
        public DbSet<GameResult> GameResults { get; set; }
    }
}
