﻿using Microsoft.EntityFrameworkCore;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Kanin_Server.Models.Entities
{
    [Index(nameof(Username))]
    public class GameResult
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public int GameId { get; set; }
        [MaxLength(255)]
        public string Username { get; set; }
        public int Score { get; set; }
        public float Placement { get; set; }
        public int NumPlayers { get; set; }
    }
}
