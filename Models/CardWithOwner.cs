﻿namespace Kanin_Server.Models
{
    public class CardWithOwner
    {
        public Card Card { get; set; }
        public string Owner { get; set; }
    }
}
