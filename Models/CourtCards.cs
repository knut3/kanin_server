﻿namespace Kanin_Server.Models
{
    public static class CourtCards
    {
        public const int Jack = 11;
        public const int Queen = 12;
        public const int King = 13;
    }
}
