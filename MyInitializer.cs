﻿using Kanin_Server.Hubs;
using Kanin_Server.Models;
using Kanin_Server.Services;
using System.Text.Json;

public class MyInitializer : IHostedService
{
    public static string STORED_GAME_STATES_FILENAME = "StoredGameStates.json";
    public Task StartAsync(CancellationToken cancellationToken)
    {
        if (File.Exists(STORED_GAME_STATES_FILENAME)){
            string json = File.ReadAllText(STORED_GAME_STATES_FILENAME);
            var options = new JsonSerializerOptions
            {
                Converters = { new PlayerJsonConverter() },
                WriteIndented = true                
            };
            List<Game> storedGames = JsonSerializer.Deserialize<List<Game>>(json, options);
            GameService.AddGameStates(storedGames);
            File.Delete(STORED_GAME_STATES_FILENAME);
        }

        return Task.CompletedTask;
    }

    public Task StopAsync(CancellationToken cancellationToken)
    {
        return Task.CompletedTask;
    }
}