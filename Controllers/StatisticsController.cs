﻿using Kanin_Server.Models;
using Kanin_Server.Models.Entities;
using Kanin_Server.Models.Responses;
using Kanin_Server.Services;
using Microsoft.AspNetCore.Mvc;

namespace Kanin_Server.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class StatisticsController : ControllerBase
    {
        private readonly ILogger<StatisticsController> _logger;
        private KaninContext _context;
        private ScoreService _scoreService;

        public StatisticsController(ILogger<StatisticsController> logger, KaninContext context)
        {
            _logger = logger;
            _context = context;
            _scoreService = new ScoreService(context, logger);
        }

        [HttpGet]
        [Route("earned-magick-cards")]
        public IActionResult GetEarnedMagickCards(string accessToken)
        {
            UserLogin? userLogin = _context.Users.SingleOrDefault(x => x.AccessToken == accessToken);
            if (userLogin == null)
            {
                _logger.LogWarning("Unathorized attempt at GetEarnedMagickCards()");
                return BadRequest("Unautorized");
            }

            var cards = _context.EarnedTarotCards
                .Where(x => x.Username == userLogin.Name)
                .GroupBy(x => new { x.Card.Id, x.Reason })
                .Where(x => x.Count() > 0)
                .Select(x => new { Card = x.First().Card, Reason = x.Key.Reason, Count = x.Count() });

            var result = cards.ToList().DistinctBy(x => x.Card.Id).Select(x => new EarnedTarotCardStatisticModel
            {
                Card = x.Card
            }).ToList();

            foreach (var item in cards)
            {
                var resultItem = result.Single(x => x.Card.Id == item.Card.Id);
                if (item.Reason == "winner")
                {
                    resultItem.ReasonWinnerCount = item.Count;
                }
                else if (item.Reason == "rabbit"){
                    resultItem.ReasonRabbitCount = item.Count;
                }
            }

            _logger.LogInformation("Successfully returning {num} magick cards to {username}", result.Count, userLogin.Name);
            return Ok(result);

        }
    }
}
