using Kanin_Server.Models;
using Kanin_Server.Models.Entities;
using Kanin_Server.Models.Input;
using Kanin_Server.Services;
using Microsoft.AspNetCore.Mvc;

namespace Kanin_Server.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class UserController : ControllerBase
    {

        private readonly ILogger<UserController> _logger;
        private KaninContext _context;
        private AuthenticationService _authService;

        public UserController(ILogger<UserController> logger, KaninContext context)
        {
            _logger = logger;
            _context = context;
            _authService = new AuthenticationService(context);
        }

        [HttpGet]
        [Route("is-available")]
        public IActionResult IsAvailable(string userName)
        {
            bool alreadyExists = _context.Users.Any(user => user.Name == userName.Trim());
            bool isBotName = BotService.BOT_NAMES.Contains(userName.Trim());
            _logger.LogInformation("IsAvailable({userName})={result}", userName, (!alreadyExists).ToString());
            return Ok(!alreadyExists && !isBotName);
        }

        [HttpGet]
        [Route("is-valid-token")]
        public IActionResult IsValidToken(string accessToken)
        {
            if (accessToken == null)
            {
                _logger.LogWarning("IsValidToken called with null argument");
                return BadRequest();
            }

            if (_context.Users.Any(u => u.AccessToken == accessToken))
            {
                return Ok(true);
            }
            else
            {
                return Ok(false);
            }
        }

        [HttpPost]
        [Route("register")]
        public IActionResult Register(RegistrationModel model)
        {
            if (!ModelState.IsValid)
            {
                _logger.LogWarning("Register called with invalid input model: {username} {password} ", model.UserName, model.Password);
                return BadRequest();
            }

            string usernameTrimmed = model.UserName.Trim();

            if (_context.Users.Any(user => user.Name == usernameTrimmed) || BotService.BOT_NAMES.Contains(usernameTrimmed))
            {
                _logger.LogWarning("Trying to register with occupied username: {username}", usernameTrimmed);
                return BadRequest(usernameTrimmed + " is not available");
            }

            string accessToken = AuthenticationService.CreateAccessToken();

            string hash = AuthenticationService.GetHash(model.Password);

            UserLogin user = new UserLogin()
            {
                AccessToken = accessToken,
                Name = usernameTrimmed,
                PasswordHash = hash
            };

            _context.Add(user);
            _context.SaveChanges();

            _logger.LogInformation("New user created: {userName}", usernameTrimmed);
            return Ok(accessToken);

        }

        [HttpPost]
        [Route("login")]
        public IActionResult Login(LoginModel model)
        {
            string hash = AuthenticationService.GetHash(model.Password);
            UserLogin? user = _context.Users.FirstOrDefault(user => 
                        user.Name == model.UserName.Trim() &&
                        user.PasswordHash == hash);

            if (user == null)
            {
                _logger.LogWarning("Login attempt, invalid username or password for {username}", model.UserName.Trim());
                return BadRequest("Invalid username or password");
            }

            _logger.LogInformation("{username} logged in", user.Name);
            return Ok(user.AccessToken);
        }
    }
}