﻿using Kanin_Server.Models;
using Kanin_Server.Models.Entities;
using Kanin_Server.Models.Responses;
using Kanin_Server.Services;
using Microsoft.AspNetCore.Mvc;

namespace Kanin_Server.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ScoreController : ControllerBase
    {
        private readonly ILogger<ScoreController> _logger;
        private KaninContext _db;
        private ScoreService _scoreService;

        public ScoreController(ILogger<ScoreController> logger, KaninContext context)
        {
            _logger = logger;
            _db = context;
            _scoreService = new ScoreService(context, logger);
        }

        [HttpGet]
        [Route("total")]
        public IActionResult GetTotal(int gameId, string accessToken)
        {
            

            var result = new GameResultModel
            {
                TotalScores = _scoreService.getTotalScores(gameId)
            };

            UserLogin? userLogin = _db.Users.SingleOrDefault(x => x.AccessToken == accessToken);
            if (userLogin == null)
            {
                _logger.LogWarning("Unathorized attempt at GetTotal()");
                return BadRequest("Unautorized");
            }

            var earnedTarotCards = _db.EarnedTarotCards
                .Where(x => x.GameId == gameId && x.Username == userLogin.Name)
                .ToList();

            result.SetEarnedTarotCards(earnedTarotCards);
           

            _logger.LogInformation("Successfully returning total scores for game with id {gameId}", gameId);

            return Ok(result);
        }

        /*
        [HttpPost]
        [Route("populate")]
        public IActionResult Populate()
        {
            var scoreService = new ScoreService(_db, _logger);

            var totalScores = _db.RoundScores.GroupBy(x => new { x.GameId, x.Username })
                .Select(x => new
                {
                    x.Key.GameId, 
                    x.Key.Username,
                    Score = x.Sum(x => x.Score)
                });

            foreach (var gameScores in totalScores.ToList().GroupBy(x => x.GameId))
            {
                var dict = gameScores.ToDictionary(x => x.Username, x => x.Score);
                scoreService.createGameResults(dict, gameScores.Key, gameScores.Count());
            }
            _db.SaveChanges();
            return Ok();
        }
        */

        [HttpGet]
        [Route("leaderboard/lowest-score")]
        public IActionResult GetLowestScoreLeaderboard(int numResults, string accessToken)
        {
            UserLogin? userLogin = _db.Users.SingleOrDefault(x => x.AccessToken == accessToken);
            if (userLogin == null)
            {
                _logger.LogWarning("Unathorized attempt at GetLowestScoreLeaderboard()");
                return BadRequest("Unautorized");
            }

            const int MAX_NUM_RESULTS = 10;
            numResults = Math.Min(numResults, MAX_NUM_RESULTS);

            var games = _db.GameResults
                .OrderBy(x => x.Score);

            var leaderBoard = games.Take(numResults).ToList().Select((x, i) => new LowestScoreModel
            {
                Ranking = i + 1,
                GameId = x.GameId,
                Username = x.Username,
                Score = x.Score
            });


            var yourBest = games.FirstOrDefault(x => x.Username == userLogin.Name);

            var result = new LowestScoreLeaderboardResult
            {
                Leaderboard = leaderBoard.ToList(),
                You = yourBest != null ? new LowestScoreModel
                {
                    GameId = yourBest.GameId,
                    Username = userLogin.Name,
                    Score = yourBest.Score,
                    Ranking = games.Count(x => x.Score < yourBest.Score) + 1
                }
                : null
            };

            fixRankingEqualScores(result.Leaderboard);

            return Ok(result);
        }

        private void fixRankingEqualScores(List<LowestScoreModel> leaderboard)
        {
            LowestScoreModel previous = null;
            foreach (LowestScoreModel score in leaderboard)
            {
                if (previous != null && score.Score == previous.Score)
                {
                    score.Ranking = previous.Ranking;
                }
                previous = score;
            }
        }

        [HttpGet]
        [Route("leaderboard/average-placement")]
        public IActionResult GetAveragePlacementLeaderboard(int numResults, string accessToken)
        {
            UserLogin? userLogin = _db.Users.SingleOrDefault(x => x.AccessToken == accessToken);
            if (userLogin == null)
            {
                _logger.LogWarning("Unathorized attempt at GetAveragePlacementLeaderboard()");
                return BadRequest("Unautorized");
            }

            const int MINIMUM_GAMES_PLAYED = 4;
            const int MAX_NUM_RESULTS = 10;
            numResults = Math.Min(numResults, MAX_NUM_RESULTS);

            

            var final = _db.GameResults.GroupBy(x => x.Username)
                .Where(x => x.Count() >= MINIMUM_GAMES_PLAYED)
                .Select(x => new
                    { 
                        Username = x.Key, 
                        AveragePlacement = x.Sum(u => u.Placement) / x.Sum(u => u.NumPlayers+1) 
                    }).ToList()
                .OrderBy(x => x.AveragePlacement)
                .Take(numResults)
                .Select((x, i) => new AveragePlacementModel
                {
                    Ranking = i + 1,
                    Username = x.Username,
                    AveragePlacement = x.AveragePlacement
                });

            var result = new AveragePlacementLeaderboardResult
            {
                MinimumGamesPlayed = MINIMUM_GAMES_PLAYED,
                Leaderboard = final.ToList(),
                You = final.FirstOrDefault(x => x.Username == userLogin.Name)
            };  
            
            fixRankingEqualAveragePlacement(result.Leaderboard);

            return Ok(result);
        }

        private void fixRankingEqualAveragePlacement(List<AveragePlacementModel> leaderboard)
        {
            AveragePlacementModel previous = null;
            foreach (AveragePlacementModel score in leaderboard)
            {
                if (previous != null && score.AveragePlacement == previous.AveragePlacement)
                {
                    score.Ranking = previous.Ranking;
                }
                previous = score;
            }
        }

        private class AveragePlacementLeaderboardResult
        {
            public int MinimumGamesPlayed { get; set; }
            public List<AveragePlacementModel> Leaderboard { get; set; }
            public AveragePlacementModel? You { get; set; }
        }
        
        private class AveragePlacementModel
        {
            public int Ranking { get; set; }
            public string Username { get; set; }
            public float AveragePlacement { get; set; }
        }

        private class LowestScoreLeaderboardResult
        {
            public List<LowestScoreModel> Leaderboard { get; set; }
            public LowestScoreModel? You { get; set; }
        }

        private class LowestScoreModel
        {
            public int Ranking { get; set; }
            public int GameId { get; set; }
            public string Username { get; set; }
            public int Score { get; set; }
        }
    }
}
